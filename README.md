# [m1dslille](https://gitlab.com/ktzanev/m1dslille) / [web page](https://ktzanev.gitlab.io/m1dslille/)

Tutorials and lectures for Probability 1 course of M1 Data Science - Université de Lille and Centrale Lille

## 2021/22

You can obtain [this repository](https://gitlab.com/ktzanev/m1dslille) by two easy ways:

- by downloading the archive [zip](https://gitlab.com/ktzanev/m1dslille/-/archive/master/m1dslille-master.zip) which contains the latest version of the files,
- retrieve the entire repository, including the change history, using `git` with the command

  ```shell
  git clone https://gitlab.com/ktzanev/m1dslille.git .
  ```

In [this repository](https://gitlab.com/ktzanev/m1dslille) you can find the following LaTeX sources and PDFs (compiled with [Tectonic](https://tectonic-typesetting.github.io/)):

- Lectures
  - Lecture n°1 **[[pdf:animated](lectures/M1DS_2021-22_Lecture1_anim.pdf)]** [[pdf:non animated](lectures/M1DS_2021-22_Lecture1_noanim.pdf)] [[pdf:to print](lectures/M1DS_2021-22_Lecture1_print.pdf)] [[tex](lectures/M1DS_2021-22_Lecture1.tex)]
  - Lecture n°2 **[[pdf:animated](lectures/M1DS_2021-22_Lecture2_anim.pdf)]** [[pdf:non animated](lectures/M1DS_2021-22_Lecture2_noanim.pdf)] [[pdf:to print](lectures/M1DS_2021-22_Lecture2_print.pdf)] [[tex](lectures/M1DS_2021-22_Lecture2.tex)]
  - Lecture n°3 **[[pdf:animated](lectures/M1DS_2021-22_Lecture3_anim.pdf)]** [[pdf:non animated](lectures/M1DS_2021-22_Lecture3_noanim.pdf)] [[pdf:to print](lectures/M1DS_2021-22_Lecture3_print.pdf)] [[tex](lectures/M1DS_2021-22_Lecture3.tex)]
  - Lecture n°4 **[[pdf:animated](lectures/M1DS_2021-22_Lecture4_anim.pdf)]** [[pdf:non animated](lectures/M1DS_2021-22_Lecture4_noanim.pdf)] [[pdf:to print](lectures/M1DS_2021-22_Lecture4_print.pdf)] [[tex](lectures/M1DS_2021-22_Lecture4.tex)]
  - Lecture n°5 **[[pdf:animated](lectures/M1DS_2021-22_Lecture5_anim.pdf)]** [[pdf:non animated](lectures/M1DS_2021-22_Lecture5_noanim.pdf)] [[pdf:to print](lectures/M1DS_2021-22_Lecture5_print.pdf)] [[tex](lectures/M1DS_2021-22_Lecture5.tex)]
  - Lecture n°6 **[[pdf:animated](lectures/M1DS_2021-22_Lecture6_anim.pdf)]** [[pdf:non animated](lectures/M1DS_2021-22_Lecture6_noanim.pdf)] [[pdf:to print](lectures/M1DS_2021-22_Lecture6_print.pdf)] [[tex](lectures/M1DS_2021-22_Lecture6.tex)]
  - Lecture n°7 **[[pdf:animated](lectures/M1DS_2021-22_Lecture7_anim.pdf)]** [[pdf:non animated](lectures/M1DS_2021-22_Lecture7_noanim.pdf)] [[pdf:to print](lectures/M1DS_2021-22_Lecture7_print.pdf)] [[tex](lectures/M1DS_2021-22_Lecture7.tex)]

_To compile these lectures you need the class file [m1ds_beamer.cls](lectures/m1ds_beamer.cls) as well as the style [beameralt.sty](lectures/beameralt.sty)._

- Tutorials
  - Tutorial n°1 **[[pdf](tutorials/M1DS_2021-22_Tutorial1.pdf)]** [[tex](tutorials/M1DS_2021-22_Tutorial1.tex)]
  - Tutorial n°2 **[[pdf](tutorials/M1DS_2021-22_Tutorial2.pdf)]** [[tex](tutorials/M1DS_2021-22_Tutorial2.tex)]
  - Tutorial n°3 **[[pdf](tutorials/M1DS_2021-22_Tutorial3.pdf)]** [[tex](tutorials/M1DS_2021-22_Tutorial3.tex)]
  - Tutorial n°4 **[[pdf](tutorials/M1DS_2021-22_Tutorial4.pdf)]** [[tex](tutorials/M1DS_2021-22_Tutorial4.tex)]
  - Tutorial n°5 **[[pdf](tutorials/M1DS_2021-22_Tutorial5.pdf)]** [[tex](tutorials/M1DS_2021-22_Tutorial5.tex)]
  - Tutorial n°6 **[[pdf](tutorials/M1DS_2021-22_Tutorial6.pdf)]** [[tex](tutorials/M1DS_2021-22_Tutorial6.tex)]

_To compile these tutorials you need the style sheet [M1DS.sty](tutorials/M1DS.sty) as well as the [department logo](tutorials/ul-fst-math_noir.pdf) and the [logo of Centrale](tutorials/logo_centrale_lille_bw.pdf)._

- Exam
  - Final Exam **[[subject pdf](exam/M1DS_2021-22_Exam_subject.pdf)]** **[[solutions pdf](exam/M1DS_2021-22_Exam_solutions.pdf)]** [[tex](exam/M1DS_2021-22_Exam.tex)]
  - Retake **[[subject pdf](exam/M1DS_2021-22_Retake_subject.pdf)]** **[[solutions pdf](exam/M1DS_2021-22_Retake_solutions.pdf)]** [[tex](exam/M1DS_2021-22_Retake.tex)]


# Old versions

## 2020/21

You can get the 2020 files in two simple ways:

- by downloading the archive [zip](https://gitlab.com/ktzanev/m1dslille/-/archive/v2020/m1dslille-master.zip) which contains the 2019 version of the files,
- retrieve the repository, in 'detached HEAD' state at tag `v2020`, using `git` with the command

  ```shell
  git clone -b v2020 --depth 1 https://gitlab.com/ktzanev/m1dslille.git .
  ```

In [this repository](https://gitlab.com/ktzanev/m1dslille/-/tree/v2020) you can find the following LaTeX sources and PDFs (compiled with XeLaTeX):

- Lectures
  - Lecture n°1 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture1_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture1_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture1_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture1.tex)]
  - Lecture n°2 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture2_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture2_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture2_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture2.tex)]
  - Lecture n°3 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture3_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture3_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture3_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture3.tex)]
  - Lecture n°4 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture4_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture4_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture4_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture4.tex)]
  - Lecture n°5 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture5_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture5_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture5_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture5.tex)]
  - Lecture n°6 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture6_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture6_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture6_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture6.tex)]
  - Lecture n°7 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture7_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture7_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture7_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/M1DS_2020-21_Lecture7.tex)]

_To compile these lectures you need the class file [m1ds_beamer.cls](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/m1ds_beamer.cls) as well as the style [beameralt.sty](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/lectures/beameralt.sty)._

- Tutorials
  - Tutorial n°1 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial1.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial1.tex)]
  - Tutorial n°2 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial2.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial2.tex)]
  - Tutorial n°3 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial3.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial3.tex)]
  - Tutorial n°4 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial4.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial4.tex)]
  - Tutorial n°5 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial5.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial5.tex)]
  - Tutorial n°6 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial6.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS_2020-21_Tutorial6.tex)]

_To compile these tutorials (and the exam) you need the style sheet [M1DS.sty](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/M1DS.sty) as well as the [department logo](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/ul-fst-math_noir.pdf) and the [logo of Centrale](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/tutorials/logo_centrale_lille_bw.pdf)._

- Exam
  - Final Exam **[[subject pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/exam/M1DS_2020-21_Exam_subject.pdf)]** **[[solutions pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/exam/M1DS_2020-21_Exam_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/exam/M1DS_2020-21_Exam.tex)]
  - Retake **[[subject pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/exam/M1DS_2020-21_Retake_subject.pdf)]** **[[solutions pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/exam/M1DS_2020-21_Retake_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2020/exam/M1DS_2020-21_Retake.tex)]


## 2019/20

You can get the 2019 files in two simple ways:

- by downloading the archive [zip](https://gitlab.com/ktzanev/m1dslille/-/archive/v2019/m1dslille-master.zip) which contains the 2019 version of the files,
- retrieve the repository, in 'detached HEAD' state at tag `v2019`, using `git` with the command

  ```shell
  git clone -b v2019 --depth 1 https://gitlab.com/ktzanev/m1dslille.git .
  ```

In [this repository](https://gitlab.com/ktzanev/m1dslille/-/tree/v2019) you can find the following LaTeX sources and PDFs (compiled with XeLaTeX):

- Lectures
  - Lecture n°1 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture1_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture1_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture1_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture1.tex)]
  - Lecture n°2 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture2_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture2_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture2_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture2.tex)]
  - Lecture n°3 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture3_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture3_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture3_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture3.tex)]
  - Lecture n°4 **[[pdf:animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture4_anim.pdf)]** [[pdf:non animated](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture4_noanim.pdf)] [[pdf:to print](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture4_print.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/M1DS_2019-20_Lecture4.tex)]

_To compile these lectures you need the class file [m1ds_beamer.cls](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/m1ds_beamer.cls) as well as the style [beameralt.sty](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/lectures/beameralt.sty)._

- Tutorials
  - Tutorial n°1 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial1.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial1.tex)]
  - Tutorial n°2 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial2.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial2.tex)]
  - Tutorial n°3 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial3.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial3.tex)]
  - Tutorial n°4 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial4.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial4.tex)]
  - Tutorial n°5 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial5.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial5.tex)]
  - Tutorial n°6 **[[pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial6.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS_2019-20_Tutorial6.tex)]

_To compile these tutorials and the exams you need the style sheet [M1DS.sty](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/M1DS.sty) as well as the [department logo](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/ul-fst-math_noir.pdf) and the [logo of Centrale](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/tutorials/logo_centrale_lille_bw.pdf)._

- Exam
  - Final Exam **[[subject pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/exam/M1DS_2019-20_Exam_subject.pdf)]** **[[solutions pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/exam/M1DS_2019-20_Exam_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/exam/M1DS_2019-20_Exam.tex)]
  - Retake **[[subject pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/exam/M1DS_2019-20_Retake_subject.pdf)]** **[[solutions pdf](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/exam/M1DS_2019-20_Retake_solutions.pdf)]** [[tex](https://glcdn.githack.com/ktzanev/m1dslille/raw/v2019/exam/M1DS_2019-20_Retake.tex)]

---
[Licence MIT](LICENSE)
