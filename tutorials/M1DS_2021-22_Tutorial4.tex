\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M1DS}

\begin{document}


\hautdepage{Tutorial 4 - Conditional probability}


% ==================================
\section{Conditional Probability}
% ==================================


%-----------------------------------
\begin{exo}[.49]

  An urn contains $10$ yellow tokens, $5$ white and $1$ red. I pulled a token from this urn and I tell you it's not red. What is the probability of it being yellow?
\end{exo}

%-----------------------------------
\begin{exo}

  A tennis player has a $40\%$ chance of passing his first serve ball. If he fails, his probability of passing his second ball is $70\%$. When his first serve ball passes, his probability of winning the point is $80\%$, while his probability of winning the point when he passes his second serve ball is only $50\%$.

  \begin{enumerate}
    \item Calculate the probability that he will make a double fault.
    \item Calculate the probability that he will lose the point on his service.
    \item Knowing that he lost the point, what is the probability that it was on a double fault?
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}

  We study a disease that affects $1\%$ of the population. There is a screening test for this disease that has the following characteristics:
  \begin{itemize}
    \item if the person is sick, the test is positive with a probability of $99 \%$;
    \item if the person is not sick, the test is positive with a probability of $9 \%$.
  \end{itemize}
  In the following, we will note $S$ the event "the person is sick" and $T$ the event "the test is positive".

  \begin{enumerate}
    \item Translate the statement data by giving, without any calculation, the value of each of the following quantities: $\P_S(T),$ $\P_{\overline S}(T)$ et $\PP{S}$.
    \item Compute $\PP{T}$.
    \item A person has performed the test, which is positive. What is the probability of her being sick?
    \item Deduce the false positive rate $\P_T(\overline S)$ for this medical test.
    \item What is the false negative rate $\P_{\overline T}(S)$?
  \end{enumerate}

\end{exo}

%-----------------------------------
\begin{exo}

  Consider a sample space of eight equiprobable sample points. Find three events $A_1$, $A_2$ and $A_3$ each of probability $\dsfrac{1}{2}$ such that:
  \begin{itemize}
    \item $\P(A_1\cap A_2\cap A_3 ) = \P(A_1)\P(A_2)\P(A_3)$,
    \item $\P(A_1\cap A_2) = \P(A_1\cap A_3) = \frac{1}{4}$,
    \item $\P(A_2\cap A_3) = \frac{1}{8}$.
  \end{itemize}
  Are $A_1$ ,$A_2$ and $A_3$ independent ?
\end{exo}

%-----------------------------------
\begin{exo}

  We roll two dice and consider the events:
  \begin{eqnarray*}
    A & = & \{\text{the result of the first die is odd}\},\\
    B & = & \{\text{the result of the second die is even}\},\\
    C & = & \{\text{the results of the two dice are of the same parity}\}.
  \end{eqnarray*}
  Study the independence two by two of the events $A$, $B$ and $C$, then the mutual independence (independence of the family) $A,B,C$.
\end{exo}

% -----------------------------------
\begin{exo}

  Consider the probability space $(\Omega,{\mathcal{F}},\mathbb{P})$, an arbitrary event $A$, and another event $B$ such that $0<\PP{B}<1$.
  \begin{enumerate}
    \item Show that
    \begin{equation}\label{indeq}
      \abs{\PP{A\cap B}-\PP{A}\PP{B}} \leq \frac{1}{4}\abs{\PP{A\mid B} - \PP{A\mid B^c}}.\tag*{$\circledast$}
    \end{equation}\vskip .21\baselineskip
    \begin{indication}
      Start by calculating $\PP{A\mid B} - \PP{A\mid B^c}$ using $\PP{A\cap B}$, $\PP{A}$, $\PP{B}$.
    \end{indication}
    \item In which case \ref{indeq} is an equality?
    \item What becomes~\ref{indeq} when $A\subset B$ ?
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}

   Two events $A$ and $B$ are said to be \emph{conditionally independent}, given another event $C$ with $\P(C) > 0$, if $\P(A \cap B | C) = \P(A|C)\P(B |C)$.
  \begin{enumerate}
    \item Prove that if $\PP{B \cap C} > 0$ then $A$ and $B$ are conditionally independent given $C$ iff
    \[
      \PP{A|B \cap C} = \PP{A|C}.
    \]
    \item Give an example of three events $A$,$B$ and $C$ such that $A$ and $B$ are independent but not conditionally independent given $C$.
  \end{enumerate}
\end{exo}


% ==================================
\section{Random vectors and independence}
% ==================================

%-----------------------------------
\begin{exo}

  \sidebyside{11cm}{
    Throughout this exercise, the discrete random vector $(X,Y)$ is supported by $S$, all six points shown in the opposite figure. The law of $(X,Y)$ is therefore given by the values of $\P\big((X,Y)=(i,j)\big)$, for $(i,j)\in S$.
  }{
    \vspace{-7mm}\includegraphics{M1DS_2021-22_Tutorial4_fig1.pdf}
  }
  \begin{enumerate}
    \item What probabilities should be assigned to the different points of $S$ to simultaneously satisfy the following two conditions:
    \begin{enumerate}
      \item the points of $\{2,3\}^2$ all have the same probability,
      \item $X$ and $Y$ are uniform on  $\{0,1,2,3\}$ ?
    \end{enumerate}
    \item When $(X,Y)$ follows the law determined in the previous question, are $X$ and $Y$ independent?
    \item Show that there are an infinitely many laws of $(X,Y)$ such that $X$ and $Y$ are uniform on $\{0,1,2,3\}$. And that in all these cases $X$ and $Y$ are dependent. Are they correlated?
    \item Determine the laws of $(X,Y)$ such that $X$ and $Y$ are independent.
  \end{enumerate}
\end{exo}


%-----------------------------------
\begin{exo}

  Consider a random vector $X=(X_1,X_2) \in \mathbb{R}^2$ with support $C=\partial S$, the 4 boundary edges of the square $S = [-1,1]^{2}$. The law of $X$ is the uniform ($1$ dimensional) probability on $C$.
  \begin{enumerate}
    \item Are $X_1$ and $X_2$ independent?
    \item Determine the law of $X_{1}X_{2}$. Is $X_1X_2$ continuous?
    \item Are $X_1$ and $X_2$ correlated?
    \item What is the covariance matrix of $X$ ?
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}

  Consider a random vector $X=(X_1,X_2) \in \mathbb{D}\subset\mathbb{R}^2$ whose distribution is the uniform distribution on the unit disk $\mathbb{D}$.
  \begin{enumerate}
    \item Are $X_1$ and $X_2$ independent?
    \item Are $X_1$ and $X_2$ correlated?
    \item What is the covariance matrix of $X$ ?
    \item\hard Determine the law of $X_{1}X_{2}$. Is $X_1X_2$ continuous?
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}

  Consider the uniform square $\Omega=(0,1)^2 \subseteq \mathbb{R}^{2}$ and the random variable $U(\omega)=\frac{\omega}{\|\omega\|}$ for $\omega \in \Omega$.
  \begin{enumerate}
    \item Identify $U(\Omega)$. Make a drawing illustrating the result.
    \item Are the components $U_1$ and $U_2$ of $U$ independent?
  \end{enumerate}
  We introduce the real random variable $\phi(\omega)\in (0,\pi/2)$ by the identity $U(\omega)=(\cos\phi(\omega), \sin\phi(\omega))$.
  \begin{enumerate}[resume]
    \item Determine $\phi(\Omega)$. Show that the law of $\phi$ is not uniform. Determine this law.
    \item Is the $U_1$ component of $U$ independent of $\phi$?
  \end{enumerate}
\end{exo}


\end{document}
