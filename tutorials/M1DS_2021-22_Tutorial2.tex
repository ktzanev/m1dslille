\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M1DS}

\begin{document}


\hautdepage{Tutorial 2 - Random variables}


% ==================================
\section{Discrete Random Variables}
% ==================================

%-----------------------------------
\begin{exo}

  We roll two non-rigged dice.
  \begin{enumerate}
    \item The random variable $X$ is considered to be the sum of the obtained points. What is the distribution of $X$?
    \item Same question for the random variable $Y$ equal to the minimum of the two obtained results.
    \item We consider the random variable $Z = (X-7)^{2}$. What is the distribution of $Z$?
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}

  An urn contains $N$ tokens numbered from 1 to $N$. We get $m\leq N$ at random and without replacement.
  \begin{enumerate}
    \item Describe a universe $\Omega$ that equipped with the uniform distribution models this random experiment.
    \item What is the probability that all the drawn tokens have numbers less than or equal to a given $k\in \mathbb{N}$?
    \item The variable $X$ is the variable equal to the largest number of drawn tokens. Determine the distribution of $X$.
    \item Same questions with replacement between draws.
  \end{enumerate}
\end{exo}


% ==================================
\section{Cumulative Distribution Function}
% ==================================


% -----------------------------------
\begin{exo} (Graphical Interpretation of CDF)

  The random variable $X$ has the cumulative distribution function $F$ shown in the figure
  \begin{center}
    \includegraphics[height=3cm]{M1DS_2021-22_Tutorial2_fig1}
  \end{center}
  \begin{enumerate}
  \item Using the information provided by this graph, give the following probability values.
  \[
    \begin{array}{llll}
      \PP{X\leq -1},\quad        &  \PP{X=0.2},\quad       & \PP{X=0.3},\quad &
      \PP{X\geq 0.2}, \\ [0.5ex]
      \PP{X>2},\quad             &  \PP{X\in[1;1.5]},\quad & \PP{X\in[1;2]},\quad        &
      \PP{\abs{X}>1}.
    \end{array}
  \]
  \item Is $X$ a continuous random variable?
  \item Calculate the sum of the jumps of $F$. Is the random variable $X$ discrete?
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}

  Consider a real random variable $X$ of uniform distribution on $[0,1]$. Determine the distribution of the random variable $Y$ in the following cases:
  \begin{enumerate}
    \item $Y=1-X$ ;
    \item $Y=a+(b-a)X$, where $a$ and $b$ are two reals such that $a<b$.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}

  Let $X$ be a real random variable with cumulative distribution function $F_X$. We put $Z=\min(X,c)$ where $c$ is a real.
  \begin{enumerate}
    \item Calculate the cumulative distribution function $F_{Z}$ of $Z$.
    \item If $X$ is continuous random variable with density $\rho$, is $Z$ still a continuous random variable ?
  \end{enumerate}
\end{exo}


% ==================================
\section{Continuous Random Variables}
% ==================================


% -----------------------------------
\begin{exo} (Interpretation of the density graph)

  The random variable $X$ has the density function $\rho$ shown in the figure
  \begin{center}
    \includegraphics{M1DS_2021-22_Tutorial2_fig2}
  \end{center}
  \begin{enumerate}
    \item Using the information provided by this graph, give the values of the following probabilities:
    \[
      \begin{array}{lll}
        \PP{X\leq -2},\quad & \PP{X=-1},\quad & \PP{X\in[-2;0]},\\
        \PP{X>1},           & \PP{X \geq 1},              & \PP{|X| > 1}.
      \end{array}
    \]
    \item Determine the cumulative distribution function $F_{X}$ of $X$.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo}

  Consider a real random variable $X$ with cumulative distribution function $F_X$ given by
  \[
    \forall u \in \mathbb{R}, \quad
    F(u) = \int_{-\infty}^u f(t) \dd t,
      \quad \text{with} \quad
    f(t)=
      \begin{cases}
          1+t    & \text{if } t\in[-1,0],\\
          \alpha & \text{if } t\in]0,2],\\
          0      & \text{else.}
      \end{cases}
  \]
  \begin{enumerate}
    \item Give the graphical representation of $f$.
    \item Determine $\alpha$.
    \item Give the graphical representation of $F$.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo} (Rayleigh's distribution)

  Consider a real random variable $X$ of uniform distribution on $(0,1]$.
  \begin{enumerate}
    \item Remind the distribution function $F_U$ of $U$.
  \end{enumerate}
  Consider a strictly positive real $\sigma$, we define a new real random variable $X$ by
  \[
    X= \sigma \sqrt{-2 \ln U}.
  \]
  \begin{enumerate}[resume]
    \item Calculate the cumulative distribution function $F_{X}$ of $X$.
    \item Is $X$ a continuous random variable? If so, give a density of $X$.
    \item What can we deduce from this on the value of the integral $\displaystyle{\int_{[0, +\infty[} x \, \eexp^{-x^2/2\sigma^2} \dd x}$ ?
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo} (Simulation)

  Let $X$ be a random variable following an exponential distribution of parameter $a>0$, i.e is positive with density $\rho(t) = a\eexp^{-at}$ on $\mathbb{R}_{+}$, and for which we note $F$ its cumulative distribution function.
  \begin{enumerate}
    \item On what maximum interval is $F$ bijective? Determine its reciprocal $G$ over this interval. What is the generalized inverse of $F$ over $\mathbb{R}$
    \item Let $U$ be a random variable following a uniform distribution on $[0,1)$. We consider $Y \coloneqq G(U)$. What is the distribution of $Y$?
    \item To conclude, explain why it is sufficient and interesting to consider $\dfrac{-\ln U}{a}$ to simulate a random variable of exponential distribution with parameter $a>0$.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo} (A bit of random trigonometry)

  A random variable $X$ is defined using the construction shown in the figure below. The angle $\big(\overrightarrow{AO},\overrightarrow{AM}\big)$ is measured in radians as the random variable $U$ that follows a uniform distribution on $]-\pi/2,\pi/2[$. The distance $AO$ is $1$ and $X$ is the abscissa of the point $M$ on the reference line $(O,\vec{i})$: $\overrightarrow{OM}=X\vec{i}$.  The angles are oriented in a trigonometric direction, the figure is made for positive $U$. For $U=0$, $M$ coincides with the point $O$ and for $-\pi/2<U<0$, $M$ is \enquote{on the left} of $O$.
  \begin{center}
    \includegraphics{M1DS_2021-22_Tutorial2_fig3}
  \end{center}
  \begin{enumerate}
    \item Express $X$ using $U$.
    \item For real $x$, calculate $\PP{X\le x}$. This gives the cumulative distribution function of the real random variable $X$.
    \item Explain why $X$ is a continuous random variable and calculate its density. What do you recognize?
    \item What is the value of $\PP{\abs{X}\le 1}$? \emph{This question can be resolved with or without the help of the previous ones.}
  \end{enumerate}
\end{exo}

% ==================================
\section{Random Vectors}
% ==================================

%-----------------------------------
\begin{exo}

  Consider a random vector $X=(X_1,X_2) \in \mathbb{R}^2$ with support $C=\partial S$, the 4 boundary edges of the square $S = [-1,1]^{2}$. The law of $X$ is the uniform ($1$ dimensional) probability on $C$.
  \begin{enumerate}
    \item Calculate $\PP{X_1=\frac12}$ and $\PP{X_1=1}$.
    \item Determine the cumulative distribution function of $X_1$ and plot it. Is $X_1$ continuous?
    \item Give the value $\PP{X_1\in[a,b]}$ for any $-1<a\leq b<1$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}

  Consider a random vector $X=(X_1,X_2) \in \mathbb{D}\subset\mathbb{R}^2$ whose distribution is the uniform distribution on the unit disk $\mathbb{D}$.
  \begin{enumerate}
    \item Calculate $\PP{X_1=a}$ for $a \in \mathbb{R}$.
    \item Show that $X_1$ is continuous, determine its density function and plot it.
  \end{enumerate}
\end{exo}


\end{document}
