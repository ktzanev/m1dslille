\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M1DS}

\begin{document}


\hautdepage{Tutorial 1 - Probability spaces}


% ==================================
\section{Universe and events}
% ==================================


% -----------------------------------
\begin{exo} (Examples of universes)

  Describe the universe $\Omega$ that allows the following question to be answered:
  \begin{enumerate}
    \item Your teacher has three children, what is the probability that they are one girl and two boys?
    \item In poker, we draw $5$ cards at random from a $52$-game of cards. What is the probability of getting a straight flush\footnote{is a hand that contains five cards of sequential rank, all of the same suit, such as Q$\heartsuit$ J$\heartsuit$ 10$\heartsuit$ 9$\heartsuit$ 8$\heartsuit$.}, a four of a kind\footnote{also known as \emph{quads}, is a hand that contains four cards of one rank and one card of another rank, such as 4$\spadesuit$ 4$\heartsuit$ 4$\clubsuit$ 4$\diamondsuit$ Q$\diamondsuit$.}, a full house\footnote{also known as a \emph{full boat} or a \emph{boat}, is a hand that contains three cards of one rank and two cards of another rank, such as 3$\spadesuit$ 3$\heartsuit$ 3$\clubsuit$ 7$\heartsuit$ 7$\diamondsuit$}?
    \item A somewhat distracted employee randomly distributes $n$ letters in $n$ envelopes with the addresses of the recipients. What is the probability that the first letter is in the right envelope?
    \item The secretary divides the $75$ students of the math degree into three groups (regardless of the choice of students):
    \begin{itemize}
      \item She distributes students completely at random: what is the probability that the three groups are balanced?
      \item She divides students by imposing groups of the same size: what is the probability that Marie and Axel will find themselves in the same group?
    \end{itemize}
  \end{enumerate}
  You can also calculate the requested probabilities (by describing the events whose probability you are calculating as subsets of $\Omega$).
\end{exo}

% -----------------------------------
\begin{exo} (Events expressions)

  Let $E$,$F$ and $G$ be three events. Find expressions for the events that of $E$,$F$,$G$,
  \vspace{-.84\baselineskip}
  \begin{multicols}{2}
    \begin{enumerate}
      \item only $F$ occurs,
      \item both $E$ and $F$ but not $G$ occur,
      \item at least one event occurs,
      \item at least two events occur,
      \item all three events occur,
      \item none occurs,
      \item at most one occurs,
      \item at most two occur.
    \end{enumerate}
  \end{multicols}
\end{exo}


\newpage
% ==================================
\section{Probability measure}
% ==================================

% -----------------------------------
\begin{exo} (Probability space?)

  Let $A \subseteq \Omega$, $A \neq \emptyset$ and $\mathcal{F} := \mathcal{P}(\Omega)$.
  Define for $B\in \mathcal{F}$
  \[
    \PP{B} :=
    \begin{cases}
      1 & \text{if } B \cap A \neq \emptyset\\
      0 & \text{if } B \cap A = \emptyset
    \end{cases}.
  \]
  Is $(\Omega,\mathcal{F},\P)$ a probability space?
\end{exo}

%-----------------------------------
\begin{exo}(Probability on permutations)

  A somewhat distracted employee typed $N$ letters and prepared $N$ envelopes with the addresses of the recipients, but randomly distributed the letters in the envelopes. To model this \enquote{very real} situation, we choose as probability space the set $ \Omega_N $ of all permutations on $ \{ 1,\ldots,N \} $ with equiprobability $ P_N $. For $ 1 \le j \le N $, we note $ A_j $ the event \emph{\enquote{the $j$-th letter is in the right envelope}}.
  \begin{enumerate}
    \item Calculate $ P_N(A_j) $.

    \item We fix $k$ integers $i_1 < i_2 < \cdots < i_k$ between $ 1 $ and $ N $. Count all permutations $\sigma$ on $\{1,\ldots,N\}$ such that $\sigma(i_1)=i_1$, $\sigma(i_2)=i_2$, \ldots, $\sigma(i_k)=i_k$. Deduce the value of $P_N( A_{i_1} \cap A_{i_2} \cap \cdots \cap A_{i_k})$.

    \item Let $B$ be the event \emph{\enquote{at least one of the letters is in the right envelope}}. Express $B$ using $(A_j)_{j=1}^{N}$.

    \item Use the inclusion-exclusion formula to calculate $P_N(B)$ and its limit when $N$ tends to infinity.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo} (Cutting spaghetti)%\label{spagghetti}

  We choose the interval $[0,l]$ as a model for a spaghetto. We \emph{randomly} cut the spaghetto of length $l$  into three pieces. The point where the spaghetto is cut for the first time is noted $x$, and for the second time $y$. We can have $x>y$. We want to know what is the probability that we can construct a triangle with these three pieces.
  \begin{enumerate}
    \item Describe the sample space $\Omega$ of all possible cuts that models this experiment.
  \end{enumerate}
  \emph{We endow this space with the uniform probability.}
  \begin{enumerate}[resume]
    \item Under what conditions on $x$ and $y$ can we construct a triangle with these three pieces?
    \item Draw a picture of the event \enquote{we can construct a triangle with these three pieces} and then calculate the probability of this event.
    \item\hard The same questions but under the condition the triangle to be acute.
  \end{enumerate}
\end{exo}

\newpage
% ==================================
\section{Sampling uniform probability}
% ==================================

% -----------------------------------
\begin{exo} (Random unit vector)

  Consider a uniform random unit vector $X=(X_1,X_2) \in \mathbb{R}^2$, i.e. with support the unit circle $S = S(0,1)$, and the law of $X$ is the uniform ($1$ dimensional) probability on $S$.
  \begin{enumerate}
    \item If you have at your disposal the \texttt{math} library and the \texttt{random} function that returns a (pseudo) random number in $[0,1)$ following the uniform law, how can you \enquote{sample} over the distribution of $X$?
    \item What is the probability that the first coordinate of $X$ is in $[0,\frac{1}{2}]$.
  \end{enumerate}
\end{exo}

% -----------------------------------
\begin{exo} (Random sub-unit vector)

  Consider a uniform random vector $X=(X_1,X_2) \in \mathbb{D}^2$, where $\mathbb{D}^2$ is the unit disk in $\mathbb{R}^{2}$.
  \begin{enumerate}
    \item If you have at your disposal the \texttt{math} library and the \texttt{random} function that returns a (pseudo) random number in $[0,1)$ following the uniform law, how can you \enquote{sample} over the distribution of $X$?
    \item What is the probability that the first coordinate of $X$ is in $[0,\frac{1}{2}]$.
  \end{enumerate}
\end{exo}


\end{document}
