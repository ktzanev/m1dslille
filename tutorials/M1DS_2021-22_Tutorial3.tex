\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{M1DS}

\begin{document}


\hautdepage{Tutorial 3 - Estimates and classical distributions}

In this tutorial sheet ``calculate'' means ``calculate if it exists''.

% ==================================
\section{Discrete Random Variables}
% ==================================

%-----------------------------------
\begin{exo} (Uniform Discrete)

  A random variable $X$ with values in $\{x_1,x_2,\ldots,x_n\}$ is called \emph{uniform} if all the values have the same probability, i.e. the PMF\footnote{PMF is the abbreviation of \emph{probability mass function}.} is $p_{X}(x_i) = \frac{1}{n}$. Let's consider the particular case where $x_{i} = i$ for $i \in  \{1,\ldots,n\}$.
  \begin{enumerate}
    \item Describe the cumulative distribution function $F_{X}$ of $X$.
    \item Calculate the mean $\E(X)$, the variance $\V(X)$ and the standard deviation $\sigma(X)$ of $X$.
    \item Calculate the characteristic function $\varphi_{X}$ of $X$.
  \end{enumerate}
\end{exo}


%-----------------------------------
\begin{exo} (Bernoulli and Rademacher)

  A random variable $X$ with values in $\{0,1\}$ is called \emph{Bernoulli} with parameter $\lambda \in [0,1]$ if the PMF is $p_{X}(1)=\lambda$ and $p_{X}(0) = 1-\lambda$.
  \begin{enumerate}
    \item Describe the cumulative distribution function $F_{X}$ of $X$.
    \item Calculate the mean $\E(X)$, the variance $\V(X)$ and the standard deviation $\sigma(X)$ of $X$.
    \item Calculate the characteristic function $\varphi_{X}$ of $X$.
  \end{enumerate}
  Same questions for \emph{Rademacher} random variable that takes two values $\{-1,1\}$ with the same probability $\frac{1}{2}$.
\end{exo}

%-----------------------------------
\begin{exo} (Binomial)

  A random variable $X$ with values in $\mathbb{N}_{0} = \{0,1,\ldots\}$ is called \emph{binomial} with parameters $\lambda \in [0,1]$ and $n$ if its support is $\{0,\ldots,n\}$ for some $n \in \mathbb{N}$ and the PMF is given by the formula
  \[
    p_X(k) = \binom{n}{k} \lambda^k(1-\lambda)^{n-k} \text{ for } k \in \{0,\ldots,n\}.
  \]
  \begin{enumerate}
    \item Show that this formula defines a PMF.
    \item Show that the sum of $n$ iid Bernoulli with parameter $\lambda$ is a binomial with parameters $(n,\lambda)$.
    \item Describe the cumulative distribution function $F_{X}$ of $X$.
    \item Calculate the mean $\E(X)$, the variance $\V(X)$ and the standard deviation $\sigma(X)$ of $X$.
    \item Calculate the characteristic function of $\varphi_{X}$ of $X$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Poisson)

  A random variable $X$ with values in $\mathbb{N}_{0} = \{0,1,\ldots\}$ is called \emph{Poisson} with parameter $\lambda \in [0,1]$ if its PMF is given by the formula
  \[
    p_X(k) = \eexp^{-\lambda} \frac{\lambda^k}{k!} \text{ for } k \in \mathbb{N}_0.
  \]
  \begin{enumerate}
    \item Show that this formula defines a PMF.
    \item Calculate the mean $\E(X)$, the variance $\V(X)$ and the standard deviation $\sigma(X)$ of $X$.
    \item Calculate the characteristic function of $\varphi_{X}$ of $X$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Geometric)

  A random variable $X$ with values in $\mathbb{N} = \{1,2,\ldots\}$ is called \emph{geometric} with parameter $\lambda \in (0,1)$ if its PMF is given by the formula
  \[
    p_X(k) = \lambda(1-\lambda)^{k-1} \text{ for } k \in \mathbb{N}.
  \]
  \begin{enumerate}
    \item Show that this formula defines a PMF.
    \item Calculate the mean $\E(X)$, the variance $\V(X)$ and the standard deviation $\sigma(X)$ of $X$.
    \item Calculate the characteristic function of $\varphi_{X}$ of $X$.
  \end{enumerate}
\end{exo}



% ==================================
\section{Continuous Random Variables}
% ==================================

%-----------------------------------
\begin{exo} (Uniform Continuous)

  A real random variable $X$ with values in $[a,b]$ is called \emph{uniform} if the PDF\footnote{PDF is the abbreviation of \emph{probability density function}.} is the constant function with support in $[a,b]$:
  \[
    \rho_X = \frac{1}{b-a}\ind_{[a,b]}.
  \]
  \begin{enumerate}
    \item Show that this formula defines a PDF.
    \item Describe the cumulative distribution function $F_{X}$ of $X$.
    \item Calculate the mean $\E(X)$, the variance $\V(X)$ and the standard deviation $\sigma(X)$ of $X$.
    \item Calculate the characteristic function $\varphi_{X}$ of $X$.
    \item If $X$ is uniform on $[0,1]$ what is the distribution of $aX+b$ for $a,b \in \mathbb{R}$ ? Deduce the distribution of $1-X$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Exponential)

  A positive real random variable $X$ is called \emph{exponential} with parameter $\lambda > 0$ if the PDF of $X$ is:
  \[
    \rho_X(t) = \lambda\eexp^{-\lambda t} \text{ for } t \in \mathbb{R}_{+}.
  \]
  \begin{enumerate}
    \item Show that this formula defines a PDF.
    \item Describe the cumulative distribution function $F_{X}$ of $X$.
    \item Calculate the mean $\E(X)$, the variance $\V(X)$ and the standard deviation $\sigma(X)$ of $X$.
    \item Calculate the characteristic function $\varphi_{X}$ of $X$.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo} (Cauchy)

  A real random variable $X$ is called \emph{standard Cauchy} if the PDF of $X$ is:
  \[
    \rho_X(t) = \frac{1}{\pi(1+t^2)} \text{ for } t \in \mathbb{R}.
  \]
  \begin{enumerate}
    \item Show that this formula defines a PDF.
    \item Describe the cumulative distribution function $F_{X}$ of $X$.
    \item Calculate the mean $\E(X)$, the variance $\V(X)$ and the standard deviation $\sigma(X)$ of $X$.
    \item Calculate the characteristic function $\varphi_{X}$ of $X$.
  \end{enumerate}
  What is the PDF of a random variable $\sigma X + \mu$? We call this distribution a \emph{Cauchy} distribution with parameters $(\mu, \sigma)$.
\end{exo}


\end{document}
