\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{m1ds_beamer}[4/8/2019 class pour le cours de M1 Data Science]

\newif\ifsidebar\sidebarfalse % par défaut 4:3 simplifié, sinon 16:10 avec sidebar
\DeclareOption{withsidebar}{
  \sidebartrue
}
\ifsidebar
  \PassOptionsToClass{aspectratio=1610}{beamer}
\else
  \PassOptionsToClass{aspectratio=43}{beamer}
\fi
\newcommand{\forsimple}[1]{\ifsidebar\relax\else#1\fi}
\newcommand{\fornotsimple}[1]{\ifsidebar#1\fi}


\newif\ifnegative\negativefalse % par défaut en couleur
\DeclareOption{negative}{
  \negativetrue
}

\newif\ifprint\printfalse % par défaut en couleur
\DeclareOption{print}{
  \printtrue
  \PassOptionsToClass{handout}{beamer}
}

\PassOptionsToClass{bigger}{beamer}

\DeclareOption*{
  \PassOptionsToClass{\CurrentOption}{beamer}
}
\ProcessOptions\relax

\LoadClass{beamer}
% =============================================================
\usepackage{iftex}
\ifPDFTeX % PDFLaTeX
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage{textcomp} % pour utiliser certains caractères
  \usepackage{lmodern}
\else % LuaLaTeX & XeLaTeX
  \usepackage{fontspec}
    \defaultfontfeatures{Ligatures=TeX} % pour avoir les ligatures standards
\fi
% ---------------
\usepackage{mathtools,amssymb,amsthm}
\usepackage{mathabx} % pour les doubles crochets \ldbrack, \rdbrack d'intervalles d'entiers et \leq français
\usepackage{stmaryrd} % plus de symboles biaires
\RequirePackage[english]{babel} % pour franciser LaTeX
\uselanguage{english}
\languagepath{english}
\usepackage[babel=true]{csquotes}
  \newcommand*\myquote[1]{\myemph{\enquote{#1}}}
  \newcommand*\mathquote[1]{\text{\myemph{\enquote{#1}}}}
% ---------------
\RequirePackage[e]{esvect} % pour les vecteurs

%==============================================================
\setbeamertemplate{navigation symbols}{}% pas de symboles de navigation
\setbeamercovered{transparent} % pour de la semi-transparence à la place de invisible

\ifsidebar
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ standard
  % hideothersubsections = cache les sous-sections, sauf pour la section courante
  \usetheme[hideothersubsections,width=23mm,height=3ex]{Berkeley}
  \beamer@headheight=1.5\baselineskip
  % \usetheme{Antibes}
  % \usetheme{Montpellier}
  % \usetheme[hideothersubsections,width=23mm]{Goettingen}
  % \useinnertheme{rectangles}
  % \useinnertheme{circles}
  %\usefonttheme{serif}
  %\usefonttheme{structuresmallcapsserif}
  %\setbeamertemplate{footline}[frame number] % rajout des numéros des frames
  %\useoutertheme{infolines}
  \setbeamertemplate{sidebar \beamer@sidebarside}
  {
    \insertverticalnavigation{\beamer@sidebarwidth}%
    \vfill
    \usebeamerfont{title in sidebar}\usebeamercolor[fg]{title in sidebar}%
    \hspace{7ex}
    \insertframenumber/\inserttotalframenumber
    \vspace{7ex}
  }%
  \logo{\insertshorttitle[width=\beamer@sidebarwidth,center,respectlinebreaks]\par}
\else
  %\usetheme{CambridgeUS}

  \setbeamertemplate{headline}{\nointerlineskip%
    \begin{beamercolorbox}[wd=\paperwidth,ht=2ex,dp=1ex]{secsubsec}%
      \tiny\quad\rule[-.1ex]{1.4ex}{1.4ex}\ \strut\insertsection\ $\blacktriangleright$\ \insertsubsection\hfill
      \insertframenumber/\inserttotalframenumber\qquad{}
    \end{beamercolorbox}}
  \setbeamertemplate{frametitle}{\nointerlineskip%
    \begin{beamercolorbox}[wd=\paperwidth,ht=1.47ex,dp=0.49ex,left]{frametitle}%
      \normalsize\qquad\strut\insertframetitle%
    \end{beamercolorbox}}
\fi
\ifnegative
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ inversé
  \setbeamercolor{normal text}{fg=white,bg=black}
  \setbeamercolor{structure}{fg=white}
  \setbeamercolor{alerted text}{fg=red!85!black}
  \setbeamercolor{item projected}{use=item,fg=black,bg=item.fg!35}
  \setbeamercolor*{palette primary}{use=structure,fg=structure.fg}
  \setbeamercolor*{palette secondary}{use=structure,fg=structure.fg!95!black}
  \setbeamercolor*{palette tertiary}{use=structure,fg=structure.fg!90!black}
  \setbeamercolor*{palette quaternary}{use=structure,fg=structure.fg!95!black,bg=black!80}
  \setbeamercolor*{framesubtitle}{fg=white}
  \setbeamercolor*{block title}{parent=structure,bg=black!90}
  \setbeamercolor*{block body}{fg=white,bg=black!95}
  \setbeamercolor*{block title alerted}{parent=alerted text,bg=black!15}
  \setbeamercolor*{block title example}{parent=example text,bg=black!15}
  \setbeamercolor{secsubsec}{bg=black!80,fg=white}
  \setbeamercolor{frametitle}{bg=black!90,fg=white}
  \newcommand{\term}[1]{\textcolor{red!70!white}{\emph{#1}}}
  \newcommand{\myemph}[1]{\textcolor{green!70!white}{\emph{#1}}}
  \setbeamercolor{alerted text}{fg=red!70!white}
\else
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ couleur
  \ifsidebar\else
    \setbeamercolor{secsubsec}{bg=black,fg=white}
    \setbeamercolor*{block title}{parent=structure,bg=black!4}
    \setbeamercolor*{block body}{bg=black!1}
  \fi
  \newcommand{\term}[1]{\textcolor{red!70!black}{\emph{#1}}}
  \newcommand{\myemph}[1]{\textcolor{green!70!black}{\emph{#1}}}
  \setbeamercolor{alerted text}{fg=red!70!black}
\fi


\ifprint
  % ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ pour impression
  \usepackage{pgfpages}
  \pgfpagesuselayout{4 on 1}[a4paper,border shrink=7mm,landscape]
  \usepackage{tikz}
  \ifPDFTeX % PDFLaTeX
    \def\pagehook{\AddToHook{shipout/background}}
  \else % LuaLaTeX & XeLaTeX
    \usepackage{everypage}
    \def\pagehook{\AddEverypageHook}
  \fi
  \pagehook{%
    \pgfmathsetmacro{\mpage}{int(mod(\thepage,2))}
    \ifnum \mpage=0\relax
      \begin{tikzpicture}[remember picture,overlay]
        \draw[shift={(current page.west)},line width=3.5mm,gray!14,-latex]
          (-2.1,0)--(-.21,0);
      \end{tikzpicture}%
    \fi
    \pgfmathsetmacro{\mpage}{int(mod(\thepage,4))}
    \ifnum \mpage=3\relax
      \begin{tikzpicture}[remember picture,overlay]
        \draw[shift={(current page.north east)},line width=3.5mm,gray!14,-latex]
          (2.1,1.4) -- (.21,.21);
      \end{tikzpicture}%
    \fi
  }
\fi

% ---------------
% pour changer les espacements avant et après les bloques
\addtobeamertemplate{block begin}{%
  \setlength\abovedisplayskip{3pt plus 2pt minus 2pt}%
  \setlength\belowdisplayskip{1pt}%
}{}

% ---------------
\newtheorem{defprop}[theorem]{Definition-Proposition}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{question}{Question}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}

%  -------------- Les abréviations standards

% pour les vecteurs
\usepackage[e]{esvect} % définition de \vv
\newcommand*{\vs}[1]{\vv{\mathcal{#1}}}
% set
\newcommand*{\setdef}[3][]{#1\{ #2 \;#1|\; #3 #1\}} % par exemple \setdef[\big]{x^2}{x \in \R}
% intervalle d'entiers
\newcommand*{\iintv}[1]{\ldbrack #1\rdbrack}
%valeur absolue
\newcommand*{\abs}[1]{\lvert #1 \rvert}
% probabilité, moyenne
\newcommand*{\PP}[1]{\mathbb{P}\left( #1 \right)}
\renewcommand{\P}{\mathbb{P}}
\newcommand*{\EE}[1]{\mathbb{E}\left( #1 \right)}
\newcommand*{\E}{\mathbb{E}}
\newcommand*{\VV}[1]{\mathbb{V}\left( #1 \right)}
\newcommand*{\V}{\mathbb{V}}
\DeclareMathOperator{\cov}{cov}
% Produit scalaire à utiliser ainsi : \scalprod uv, \scalprod{\vv u}{\vv v}, \scalprod[\big]{\vv A}{\vv B}
\newcommand*{\scalprod}[3][]{#1\langle{#2}\kern1pt #1|{#3}#1\rangle}
% norm
\newcommand*{\norm}[1]{\left\|#1\right\|}
% direct sum
\newcommand*{\poplus}{\overset{\scriptscriptstyle\perp}{\oplus}}
% l'exponentiel
\DeclareMathOperator{\eexp}{e}
% la fonction indicatrice
\usepackage{dsfont}
\newcommand*{\ind}{\mathds{1}}
% le «d» du dx, dt, ...
% \let\d\relax\DeclareMathOperator{\d}{d}
\newcommand{\dd}{\,\mathrm{d}}
% les fractions comme \sfrac
\usepackage{xfrac}
\newcommand*{\dsfrac}[2]{\raisebox{-.1em}{\scalebox{1.4}{\sfrac{#1}{#2}}}} % bigger \sfrac
% autres
\let\iff\Leftrightarrow % ou \newcommand\iff{\;\Leftrightarrow\;}
\newcommand*\isoto{\xrightarrow{\smash{\raisebox{-.28em}{\ensuremath{\sim\kern1pt}}}}}
\DeclareMathOperator{\card}{card}
\let\ker\relax\DeclareMathOperator{\ker}{Ker}
\DeclareMathOperator{\im}{Im}
\DeclareMathOperator{\signe}{signe}
\newcommand*{\id}{\mathrm{Id}}
\DeclareMathOperator{\trace}{tr}
\newcommand*{\tr}[1]{#1^{\intercal}} % ou \top ?
% pour les ajustements verticaux
\newcommand*\baseup[1]{\vspace{-#1\baselineskip}}
\newcommand*\smallup{\baseup{.35}}
\newcommand*\medup{\baseup{.7}}
\newcommand*\bigup{\baseup{1}}

% correction pour la dérivé sous XeLaTeX
\AtBeginDocument{%
  \DeclareSymbolFont{pureletters}{T1}{lmss}{\mddefault}{it}%
}
% pour avoir \Alt qui preserve le plus long des textes
\usepackage{beameralt}
