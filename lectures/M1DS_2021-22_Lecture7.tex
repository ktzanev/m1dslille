% \PassOptionsToClass{negative}{m1ds_beamer}
% \PassOptionsToClass{withsidebar}{m1ds_beamer}
% \PassOptionsToClass{handout}{m1ds_beamer}
\documentclass[11pt]{m1ds_beamer}

% ---------------
\title{M1 Data Science - Introduction to Probability}
\subtitle{Lecture 7}
%\author{Kroum Tzanev}
\date{non taught in 2021}
% ---------------

\DeclareMathOperator{\myprod}{\raisebox{.21em}{\scalebox{.49}{$\boxdot$}}}

\begin{document}

% ------- The title --------
\begin{frame}
  \titlepage
\end{frame}

% ------- Warning --------
\begin{frame}
  \begin{alertblock}{Slides}
    This document contains some extra slides which could not be taught due to lack of time.
  \end{alertblock}
\end{frame}


% =======================================
\section{Convergence of variables}
% =======================================

% -----------
\begin{frame}{Types of convergence}
  Given a sequence of random vectors $X_{n}:\Omega \to \mathbb{R}^{d}$ and another (limit) random vector $X:\Omega \to \mathbb{R}^{d}$, we have the following notions of convergence $X_{n} \to X$:
  \begin{itemize}[<+(1)->]
     \item We say that $(X_{n})$ converges \myemph{almost surely} to $X$, and we note $X_{n} \xrightarrow{a.s.} X$, if
     \[
        \PP{X_{n} \to X}=1.
     \]
     \item We say that $(X_{n})$ converges \myemph{in mean} to $X$, and we note $X_{n} \xrightarrow{\mathcal{L}_1} X$, if $\EE{\norm{X_{n}}} < \infty, \forall n$, $\EE{\norm{X}} < \infty$ and
     \[
        \EE{\norm{X_{n} - X}} \to 0.
     \]
     \item We say that $(X_{n})$ converges \myemph{in probably} to $X$, and we note $X_{n} \xrightarrow{\P} X$, if
     \[
        \PP{\norm{X_{n}-X} > \varepsilon} \to 0, \qquad\forall \varepsilon > 0.
     \]
   \end{itemize}
\end{frame}

% -----------
\begin{frame}{in mean vs almost sure}
  In general no one of the convergences \myemph{in mean} and \myemph{almost sure} implies the other, but...\pause
  \begin{proposition}
    If $(X_n)$ is dominated, i.e. $\exists Z:\Omega \to \mathbb{R}_{+}$ with $\EE{Z} < \infty$ such that $\norm{X_n} \leq Z, \forall n \in \mathbb{N}$ almost everywhere,\pause{} then convergence \myemph{almost sure} implies \myemph{in mean}.
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{almost sure vs in probability}
  \begin{proposition}
    Convergence \myemph{almost sure} implies convergence \myemph{in probability}.
  \end{proposition}\pause
  This is not an equivalence in general, but...\pause
  \begin{proposition}
    If $X_{n}\xrightarrow{\P} X$, then there is a sub-sequence such that $X_{n_k} \xrightarrow{a.s.} X$.
  \end{proposition}\pause
  And if $(X_n)$ converge "rapidly" \myemph{in probability}, then the entire sequence $(X_n)$ converge \myemph{almost surely}.\pause{} More precisely
  \begin{proposition}
    If $\forall \varepsilon>0,\ \sum_{n \in \mathbb{N}} \PP{\norm{(X_n)-X}>\varepsilon} < \infty$, then $X_n \xrightarrow{a.s.} X$.
  \end{proposition}
  % If $(X_n)$ is a sum of independent vectors, then convergence in \myemph{in probability}, implies convergence \myemph{almost surely}. \pause More precisely
  % \begin{proposition}
  %   If $Y_{n}$ are independent, and $X_{n} = \sum_{k=0}^{n}Y_{n}$ then $X_n \xrightarrow{\P} X$ $\iff$ $X_n \xrightarrow{a.s.} X$.
  % \end{proposition}
\end{frame}

% -----------
\begin{frame}{in mean vs in probability}
  \begin{proposition}
    Convergence \myemph{in mean} implies convergence \myemph{in probability}.\pause
  \end{proposition}\pause
   This is not an equivalence in general, but...\pause
  \begin{proposition}
    If $(X_n)$ are uniformly bonded, i.e. $\exists a \in \mathbb{R}_{+}$ such that $\norm{X_n} \leq a, \forall n \in \mathbb{N}$ (almost everywhere),\pause{} then convergence \myemph{in probability} implies \myemph{in mean}.
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{in probability vs in distribution}
  \begin{proposition}
    Convergence \myemph{in probably} implies convergence \myemph{in distribution}.
  \end{proposition}\pause
  The reverse is not true, but...
  \begin{proposition}
    Convergence \myemph{in distribution} to a \textbf{constant} implies convergence \myemph{in probability}.
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{Law of large numbers}
  \begin{theorem}[LLN]
    Let $(X_{i})$ be i.i.d. random vectors of $\mathbb{R}^{d}$ with mean $\EE{X_i}=m$, then\pause
    \begin{align}
    \frac{1}{n}\sum_{i=1}^n (X_i) &\xrightarrow{\mathcal{P}} m,\tag{\myemph{weak LLN}}\\
    \intertext{and}
    \frac{1}{n}\sum_{i=1}^n (X_i) &\xrightarrow{a.s.} m.\tag{\myemph{strong  LLN}}
    \end{align}
  \end{theorem}
\end{frame}

% -----------
\begin{frame}{Mapping theorem}
  \begin{theorem}[Mapping theorem]
    Let $(X_n):\Omega \to \mathbb{R}^{d}$, $X:\Omega \to \mathbb{R}^{d}$ random vectors, and $f:\mathbb{R}^{d} \to \mathbb{R}^{t}$ continuous function on $C$ with $\PP{X \in C}=1$, then
    \begin{itemize}
      \item $X_n \xrightarrow{a.s.} X$ $\implies$ $f(X_n) \xrightarrow{a.s.} f(X)$;
      \item $X_n \xrightarrow{\P} X$ $\implies$ $f(X_n) \xrightarrow{\P} f(X)$;
      \item $X_n \xrightarrow{\mathcal{D}} X$ $\implies$ $f(X_n) \xrightarrow{\mathcal{D}} f(X)$.
    \end{itemize}
  \end{theorem}\pause
  \begin{remark}
    In this theorem we can relax "$f$ continuous" by $\PP{X\text{ in the domain of continuity of }f}=1$.
  \end{remark}
\end{frame}

\end{document}
