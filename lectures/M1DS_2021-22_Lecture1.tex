% \PassOptionsToClass{negative}{m1ds_beamer}
% \PassOptionsToClass{withsidebar}{m1ds_beamer}
% \PassOptionsToClass{handout}{m1ds_beamer}
\documentclass[11pt]{m1ds_beamer}

% ---------------
\title{M1 Data Science - Introduction to Probability}
\subtitle{Lecture 1}
%\author{Kroum Tzanev}
\date{November 9, 2021}
% ---------------

\begin{document}

% ------- The title --------
\begin{frame}
  \titlepage
\end{frame}

% ------- Warning --------
\begin{frame}
  \begin{alertblock}{Slides}
    This document contains only definitions and propositions. So it can be considered as detailed plan.\pause

    The main part --- explanations, proofs, examples, \ldots --- will be done on the blackboard.
  \end{alertblock}
\end{frame}

% =======================================
\section{Probability space}
% =======================================

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Terminology}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Universe}
  Let $\Omega$ be a \myemph{(finite, countable or uncountable)} set of all possible outcomes of a random experience.\pause

  \begin{block}{Dictionary}
    \begin{itemize}[<+(1)->]
      \item The set $\Omega$ of all possible outcomes is called \term{sample space} or the \term{the universe}.
      \item A subset $X \subset \Omega$ is called an \term{event}.
      \item An event $X=\{x\}$ with single element inside is called \term{elementary event}. Often \{x\} is identified with the \term{outcome} $x$.
      \item The empty set $\emptyset$ is called \term{the impossible event}, and $\Omega$ is the \term{the certain event}.\pause\\ This both events are called \term{the trivial events}.
      \item Two events $A$ and $B$ are called \term{incompatible} if $A \cap B = \emptyset$.
      \item The \term{negation} of an events $A$ is the complementary event $\bar{A} = \Omega \setminus A$.
    \end{itemize}
  \end{block}
\end{frame}

% -----------
\begin{frame}{Universe}
  \begin{block}{Terminology}
    \begin{itemize}[<+(1)->]
      \item \myquote{and} = $\cap$, \myquote{or} = $\cup$, \myquote{exclusive or} = $\sqcup$;
      \item an \myquote{exhaustive system} of events = \emph{countable} partition of $\Omega$;
      \item \myquote{implication} = $\subset$.
    \end{itemize}
  \end{block}
\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Probability: the countable case}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Probability on countable universe: definition}
  A probability on a \myemph{finite or countable} universe $\Omega$ is determined by a positive map $\P: \Omega \to [0,1]$, called \term{probability mass function (PMF)}, such that
  \[
    \sum_{x \in \Omega} \PP{x} = 1.
  \]\pause

  This map can be \enquote{extended} \emph{(we keep the same name)} to the set of all events $\P: \mathcal{P}\left(\Omega\right) \to [0,1]$ by
  \[
    \PP{A} = \sum_{x \in A} \PP{x} \text{, for any } A \subset \Omega.
  \]\pause
  \begin{definition}
    An event $X$ is called \term{impossible} (\Alt<4->{resp. \term{possible}}{}, \Alt<5->{resp. \term{certain}}{}) if $\PP{X}=0$ (\Alt<4->{resp. $\PP{X}>0$}{}, \Alt<5->{resp. $\PP{X}=1$}{})
  \end{definition}
\end{frame}

% -----------
\begin{frame}{Probability on countable universe: properties}
  \begin{block}{Properties}
    \begin{itemize}[<+(1)->]
      \item \myemph{(positivity)} $\PP{A} \geq 0$ and $\PP{A} \leq 1$ for any event $A$;
      \item $\PP{\emptyset} = 0$ (\textbf{the} impossible event) and $\PP{\Omega} = 1$ (\textbf{the} certain event);
      \item \myemph{(monotonicity)} if $A$ implies $B$ (i.e. $A \subset B$), then $\PP{A}\leq \PP{B}$;
      \item \myemph{($\sigma$-additivity)} $\PP{\bigsqcup A_{i}} = \sum \PP{A_{i}}$;
      \item $\PP{A} + \PP{\bar A} = 1$.
    \end{itemize}
  \end{block}
\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Probability: the general case}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Probability on uncountable universe}
  In the uncountable case $\sum \rightsquigarrow \int$.\pause

  \begin{definition}
    A \term{probability space} is a triplet $(\Omega,\mathcal{F},\P)$, where:

    \begin{enumerate}[<+(1)->]
      \item $\Omega$ is a space (of elementary outcomes);
      \item $\mathcal{F}$ is a $\sigma$-algebra of \myemph{measurable} subsets (events) of $\Omega$;
      \item $\P: \mathcal{F} \rightarrow [0,1]$ is a positive\pause, $\sigma$-additive function:
      \[
        \PP{\bigsqcup_{i=1}^{\infty} A_{i}} = \sum_{i=1}^{\infty} \PP{A_{i}},
      \]
      for any countable disjoin union $\left( A_{i} \right)_{i \in \mathbb{N}}$ of \myemph{measurable} events $A_{i} \in \mathcal{F}$\pause, with total mass $\PP{\Omega}=1$.\pause

      $\P$ is called a \term{probability measure}.\pause

      If $A \in \mathcal{F}$, then the number $\PP{A}$ is called \term{the probability} of $A$.
    \end{enumerate}
  \end{definition}
\end{frame}

% -----------
\begin{frame}{General probability space}
  \begin{block}{Properties}
    \begin{enumerate}[<+(1)->]
      \item If $\Omega$ is finite or countable and $\P$ is a probability on $\Omega$, then $(\Omega,\mathcal{P}(\Omega),\P)$ is a probability space.
      \item $\PP{\emptyset} = 0$ (the impossible event);
      \item \myemph{(monotonicity)} if $A$ implies $B$ (i.e. $A \subset B$), then $\PP{A}\leq \PP{B}$;
      \item $\PP{A} + \PP{\bar A} = 1$.
      \item \myemph{(upper continuity)} If $A_{1}\subseteq A_{2}\subseteq\cdots$ is an increasing sequence of events, then $\PP{\bigcup_{i=1}^{\infty}A_i} = \lim \PP{A_i} = \sup \PP{A_i}$.
      \item \myemph{(lower continuity)} If $A_{1}\supseteq A_{2}\supseteq\cdots$ is an decreasing sequence of events, then $\PP{\bigcap_{i=1}^{\infty}A_i} = \lim \PP{A_i} = \inf \PP{A_i}$.
      \item \myemph{(barycenter)} If $\P_{1}, \P_{2}$ are probabilities and $\lambda_{1},\lambda_{2} \in [0,1]$ such that $\lambda_{1}+\lambda_{2}=1$ than $\P=\lambda_{1}\P_{1}+\lambda_{2}\P_{2}$ is a probability.\pause{} And we say that $\P$ is a \term{mean} of $\P_{1}$ and $\P_{2}$.
    \end{enumerate}
  \end{block}
\end{frame}

% -----------
\begin{frame}{General probability space}
  \begin{proposition}[Inclusion-exclusion principle]
    \begin{itemize}[<+(1)->]
      \item $\PP{A \cup B} = \PP{A} + \PP{B} - \PP{A \cap B}$,
      \item for finite family $\left( A_i \right)_{i \in I}$ of events we have:
      \[
        \PP{\bigcup_{i \in I} A_i} = \sum_{\emptyset \neq S \subseteq I} (-1)^{\abs{S}-1} \PP{\bigcap_{i \in S} A_i}.
      \]
    \end{itemize}
  \end{proposition}\pause
  \begin{proposition}[Total probability]
    For an (almost) exhaustive system of events $\left( A_i \right)$ we have
      \[
        P(B) = \sum_i P(B \cap A_{i}).
      \]
  \end{proposition}
\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Probability: classical spaces}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Probability on topological space, support}
  \begin{itemize}[<+(1)->]
    \item We will consider only "nice" topological spaces $\Omega$ (like $\mathbb{N}$, $\mathbb{R}$, $\mathbb{R}^{n}$, or "nice" subspaces of this ones).
    \item In this case the $\sigma$-algebra will always be \term{$\mathcal{B}(\Omega)$}, the \term{Borel} $\sigma$-algebra of $\Omega$.
    \item Every countable set $\Omega$ (like $\mathbb{N}$) can be considered as discrete topological space, and if $\P$ is a probability on $\Omega$ then $(\Omega, \mathcal{P}(\Omega), \P )$ is a topological probability space.
    \item When $\Omega=\mathbb{R}$ we have $\mathcal{B}(\Omega) = \sigma\left( \setdef[\big]{(-\infty,t]}{t \in \mathbb{R}} \right)$\pause$\neq \mathcal{P}(\mathbb{R})$.
  \end{itemize}
  \begin{definition}
    Given a \myemph{topological} probability space $\left(\Omega,\mathcal{B}(\Omega),\P \right)$ we call the \term{support} of $\P$ the smallest closed subset $S \subseteq \Omega$ such that $\PP{S}=1$.
  \end{definition}\pause
  \begin{definition}
    An outcome $x \in \Omega$ is called an \term{atom} iff $\PP{\{x\}}\neq 0$.
  \end{definition}
\end{frame}
% -----------
\begin{frame}{Probability on uncountable universe: $\mathbb{R}^{n}$}
  \begin{block}{Remainder}
    On $\mathbb{R}\Alt<-2>{}{^n}$ there is a unique (translation invariant) measure $\lambda$, called the \term{Lebesgue measure} such that
    \[
      \Alt<-2>{%
        \lambda\left( [a,b] \right) = b-a
      }{%
        \lambda\left( [a_1,b_1]\times \cdots \times[a_n,b_n]  \right) = (b_1-a_1)\cdots(b_n-a_n)
      }.
    \]
    for all $\Alt<-2>{a,b \in \mathbb{R}}{(a_1,\dots,a_n),(b_1,\dots,b_n) \in \mathbb{R}^{n}}$.
    \pause

    This is the default measure on $\mathbb{R}\Alt<-2>{}{^n}$ and when we say that some property $P$ is true \term{almost everywhere} on $\mathbb{R}\Alt<-2>{}{^n}$ we mean that $\lambda\left( \setdef{x \in \mathbb{R}}{P(x)\text{ is false}} \right) = 0$.
  \end{block}
  \onslide<+(2)->
  \begin{block}{Uniform probability}
    For measurable subset $\Omega$ of $\mathbb{R}^{n}$ with finite Lebesgue measure $0 < \lambda(\Omega) < \infty$, the \term{uniform probability space $\Omega$} is $(\Omega,\mathcal{B}(\Omega),\frac{\lambda}{\lambda(\Omega)})$.
  \end{block}\pause
\end{frame}

% -----------
\begin{frame}{Probability on uncountable universe: $\mathbb{R}^{n}$}
  \begin{definition}
    A measure $\P$ on $\left( \mathbb{R}^{n},\mathcal{B}(\mathbb{R}^{n}) \right)$ is called:
    \begin{itemize}[<+(1)->]
      \item \term{continuous} if $\P(A) = \int_{A}\rho \dd x$ for any $A \in \mathcal{B}(\mathbb{R}^{n})$, where $\dd x \coloneqq \dd \lambda(x)$ is the standard Lebesgue measure on $\mathbb{R}^{n}$ and $\rho$ is a \term{density}: positifs measurable function with total mass $\int_{\Omega}\rho \dd x = 1$.
      \item \term{discrete} if the support of $\P$ is countable (discrete) subspace of $\mathbb{R}^{n}$;
      \item \term{singular} if $\P$ has no atoms and its support is of Lebesgue measure $0$.
    \end{itemize}
  \end{definition}
  \pause
  \begin{proposition}
    Every probability on $\left( \mathbb{R}^{n},\mathcal{B}(\mathbb{R}^{n}) \right)$ is a mean of one discrete, one continuous and one singular probabilities.
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{Probability on uncountable universe: $\mathbb{R}$}
   \begin{definition}
    For probability $\P$ on $\left( \mathbb{R},\mathcal{B}(\mathbb{R}) \right)$ we call \term{cumulative distribution function} $F_{\P}(t) = \PP{(-\infty,t]}$ for any $t \in \mathbb{R}$.
  \end{definition}
  \pause
  \begin{proposition}
    The application $\P \to F_{\P}$ is a bijection from the probabilities on $\mathbb{R}$ to non-decreasing, right-continuous functions with limits $\lim_{t \to -\infty}F_{\P}(t)=0$ and $\lim_{t \to \infty}F_{\P}(t)=1$.
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{Probability on uncountable universe: $\mathbb{R}$}
  \begin{proposition}
    Every cumulative distribution function $F_{\P}$ is derivable almost everywhere. We denote its derivative by $\rho_\P \coloneqq F'_{\P}$.
  \end{proposition}
  \pause
  \begin{proposition}
    \begin{enumerate}[<+(1)->]
      \item $\P$ is discrete iff $F_{\P}$ is piecewise constant;
      \item $\P$ is continuous iff $F_{\P}$ is continuous and $\int_{\mathbb{R}} \rho_\P = 1$;
      \item $\P$ is singular iff $F_{\P}$ is continuous and $\rho_\P = 0$ almost everywhere.
    \end{enumerate}
  \end{proposition}
\end{frame}


\end{document}
