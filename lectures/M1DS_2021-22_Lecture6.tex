% \PassOptionsToClass{negative}{m1ds_beamer}
% \PassOptionsToClass{withsidebar}{m1ds_beamer}
% \PassOptionsToClass{handout}{m1ds_beamer}
\documentclass[11pt]{m1ds_beamer}

% ---------------
\title{M1 Data Science - Introduction to Probability}
\subtitle{Lecture 6}
%\author{Kroum Tzanev}
\date{December 14, 2021}
% ---------------

\DeclareMathOperator{\myprod}{\raisebox{.21em}{\scalebox{.49}{$\boxdot$}}}
\DeclareMathOperator{\supp}{supp}

\begin{document}

% ------- The title --------
\begin{frame}
  \titlepage
\end{frame}

% ------- Warning --------
\begin{frame}
  \begin{alertblock}{Slides}
    This document contains only definitions and propositions. So it can be considered as detailed plan.\pause

    The main part --- explanations, proofs, examples, \ldots --- will be done on the blackboard.
  \end{alertblock}
\end{frame}


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Conditional expectation}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Conditional expectation : an event}
  \begin{definition}[with respect to an event]
    For a \Alt<-2>{real random variable}{random vector} $X: (\Omega,\mathcal{F},\P)\to \mathbb{R}\Alt<-2>{}{^n}$ the conditional expectation with respect to a possible event $A \subseteq \Omega$ is (if it exists) a \Alt<-2>{\myemph{real number}}{vector} $\EE{X\vert A} \in \mathbb{R}\Alt<-2>{}{^n}$ that is the expectation of $X$ seen as random variable on $(\Omega,\mathcal{F},\P_A)$:
    \[
      \EE{X\vert A} = \frac{\EE{\ind_AX}}{\PP{A}} \pause = \frac{1}{\PP{A}}\int_A X(\omega) \dd\PP{\omega} \in \mathbb{R}\Alt<-2>{}{^n}.
    \]
  \end{definition}\pause\pause
  \begin{block}{Properties}
    \begin{enumerate}[<+(1)->]
      \item If $X$ has an expectation then $\EE{X\vert A}$ is well defined fo any possible $A$.
      \item $X\equiv Y$(a.e) \pause $\iff$ $\EE{X\vert A}=\EE{Y\vert A}$ for any possible $A$.
      \item $\EE{X} = \sum \EE{X\vert A_i}\PP{A_i}$ for any (almost) exhaustive system of possible events $(A_{i})$.
    \end{enumerate}
  \end{block}
\end{frame}

% -----------
\begin{frame}{Conditional expectation : discrete variable}
  \begin{definition}[with respect to discrete variable]
    For two random variables on $(\Omega,\mathcal{F},\P)$:
    \begin{itemize}[<+(1)->]
      \item a random vector $X:\Omega\to\mathbb{R}^m$ and
      \item a discrete random variable $Y:\Omega\to\Sigma$ with support $\{y_i\}$,
    \end{itemize}\pause
    we consider the function $y_i\mapsto\EE{X \vert Y=y_i}$.\\\pause
    If it is well defined we consider:
    \begin{itemize}[<+(1)->]
      \item this function as random variable on $(\Sigma,\P_Y)$, and we denote it as $\EE{X \vert \cdot}$, or as $\EE{X \vert y}$,
      \item and its composition with $Y$, denoted by $\EE{X \vert Y}$ as a new random variable on $(\Omega,\mathcal{F},\P)$
      \pause known as \term{the conditional expectation of $X$ with respect to $Y$}.
    \end{itemize}
  \end{definition}
  \begin{block}{Properties}
    \begin{itemize}[<+(1)->]
      \item $\E(X \vert Y \in B) = \E(\E(X\vert Y)\vert Y \in B)$ for any possible $B$
      \item $\E(f(Y) \myprod X\vert Y) = f(Y) \myprod \E(X\vert Y)$, for any $f:\Sigma\to\mathbb{R}^m$
    \end{itemize}
  \end{block}
\end{frame}

% -----------
\begin{frame}{Conditional expectation : continuous case}
  \begin{definition}[continuous case]
    For two random vectors $X\in\mathbb{R}^{m}$ and $Y\in\mathbb{R}^{n}$ on $(\Omega,\mathcal{F},\P)$ such that the vector $(X,Y)\in\mathbb{R}^{m+n}$ is continuous with density $\rho(x,y)$, \pause and $f:\mathbb{R}^{m+n}\to\mathbb{R}^{p}$, \pause we consider the function
    $\displaystyle
      y\mapsto\frac{\int_{\mathbb{R}^m} f(x,y)\rho(x,y) \dd x}{\int_{\mathbb{R}^m} \rho(x,y) \dd x}
    $.\\\pause
    If it is well defined almost everywhere on $\supp Y$ we consider:
    \begin{itemize}[<+(1)->]
      \item this function as random variable on $(\mathbb{R}^n,\P_Y)$, and we denote it as $\EE{f(X,Y) \vert \cdot}$, or as $\EE{f(X,Y) \vert y}$,
      \item and its composition with $Y$, denoted by $\EE{f(X,Y) \vert Y}$ as a new random variable on $(\Omega,\mathcal{F},\P)$
      \pause known as \term{the conditional expectation of $f(X,Y)$ with respect to $Y$}.
    \end{itemize}
  \end{definition}
  \begin{block}{Properties}
    \begin{itemize}[<+(1)->]
      \item $\E(X \vert Y \in B) = \E(\E(X\vert Y)\vert Y \in B)$ for any possible $B$
      \item $\E(f(Y) \myprod X\vert Y) = f(Y) \myprod \E(X\vert Y)$, for any $f:\mathbb{R}^n\to\mathbb{R}^{m}$
    \end{itemize}
  \end{block}
\end{frame}

% -----------
\begin{frame}{Conditional expectation with respect to another variable}
  \begin{proposition}[the general case]
    For a random vector $X:\Omega\to \mathbb{R}^m$ and another random variable $Y:\Omega\to(\Sigma, \mathcal{G})$ on $(\Omega,\mathcal{F},\P)$ under \enquote{reasonable conditions} there is a unique random variable $\EE{X\vert\cdot}:\Sigma\to \mathbb{R}^m$ which verifies
    \[
      \E(X \vert Y \in B) = \E(\E(X\vert Y)\vert Y \in B) \text{ for any possible $B$.}
    \]
    \pause
    And it also verifies
    \[
      \E(f(Y) \myprod X\vert Y) = f(Y) \myprod \E(X\vert Y)
    \]
    for any $f:\Sigma\to\mathbb{R}^{k}$ and any product $\myprod : {R}^{k}\times{R}^{m}\to\mathbb{R}^{l}$.
  \end{proposition}
\end{frame}
% -----------
\begin{frame}{Conditional expectation : properties}
  Under the condition of existence of the expectations:
  \begin{enumerate}[<+(1)->]
    \item $\E(X) = \E(\E(X\vert Y))$.
    \item $\E(f(Y)\vert Y) = f(Y)$, and so $\E(Y\vert Y) = Y$.
    \item If $X$ and $Y$ are independent then $\EE{X\vert Y}\equiv\EE{X}$.
    \item $\E(\E(X\vert Y)\vert f(Y)) = \E(\E(X\vert f(Y)) \vert Y) = \E(X\vert f(Y))$.
    \item \myemph{(linearity)} $\E(\lambda_1 X_1 + \lambda_2 X_2 \vert Y) = \lambda_1\E(X_1 \vert Y)+\lambda_2\E(X_2 \vert Y)$.
    \item \myemph{(monotonicity)} If $X_{1}\leq X_{2}$ then $\E(X_1\vert Y)\leq \E(X_2\vert Y)$.
    \item \myemph{(Jensen's inequality)} For convex $f:\mathbb{R}^m\to\mathbb{R}$ we have $f(\E(X\vert Y))\leq \E(f(X)\vert Y)$.
  \end{enumerate}\pause
  \begin{theorem}[$L^2$-projection]
    If $X:\Omega\to \mathbb{R}^{m}$ is in $L^{2}(\Omega,\dd\P)$, i.e. has a second moment, then the orthogonal projection of $X$ to $\{\phi(Y) \vert \phi \in L^{2}(\Sigma,\dd\P_Y)\}$ is $\E(X\vert Y)$, \pause i.e. $\E(X\vert Y) \in L^{2}(\Omega,\dd\P)$ and for any $\phi \in L^{2}(\Sigma,\dd\P_Y)$ we have  $\E(\tr{\phi(Y)}(X-\E(X\vert Y)))=0$.
  \end{theorem}
\end{frame}

% -----------
\begin{frame}{Conditional expectation of jointly Gaussian variables}
    \begin{enumerate}[<+(1)->]
      \item Let $X$ and $Y$ be two jointly real Gaussian variables. If $Y$ is non constant then
      \[
        \E(X\vert Y) = \frac{\cov(X,Y)}{\V(Y)}(Y-\E(Y))+\E(X).
      \]\pause
      So the random variable $\E(X\vert Y)$, even if $Y$ is constant, is Gaussian.
      \item More generally if $X$ and $Y$ are two jointly Gaussian vectors, i.e. their coordinates are jointly Gaussian, and such that $\cov(Y)$ is invertible, then
      \[
        \E(X\vert Y) = \cov(X,Y)\cov(Y)^{-1}(Y-\E(Y))+\E(X).
      \]
    \end{enumerate}
\end{frame}


% =======================================
\section{Convergence of variables}
% =======================================


% -----------
\begin{frame}{Convergence in distributions}

  There are many different kind of convergences. \pause
  In this lecture we will focus on only one : the \term{convergence in distribution}.\\\pause
  For more informations on convergence of random variables, you can check the slide of the untaught lecture 7.\pause
  \begin{itemize}
    \item Every probability measure $\mu$ on a Borel space $X$ defines linear form on $C_b(X)$, the set of bounded continuous functions on $X$, by the integral $f\mapsto \int_X f(x) \dd \mu(x)$.\medskip\pause
    \item The week topology on the set of probability measures is the topology induced by this (pre)duality with $C_b(X)$. In this topology $\mu_{n} \xrightarrow{W_*} \mu$ $\iff$ $\int_X f(x) \dd \mu_n(x) \to \int_X f(x) \dd \mu(x)$  for $\forall f \in C_b(X)$.
  \end{itemize}\pause
  \begin{definition}
    We say that $X_{n} : \Omega_{n} \to \mathbb{R}^{d}$ converge \myemph{in distribution} to $X : \Omega_{n} \to \mathbb{R}^{d}$, and we note $X_{n} \xrightarrow{\mathcal{D}} X$, if $\P_{X_n} \xrightarrow{W_*} \P_{X}$,\\\pause
    i.e. if $\forall f \in C_b(X)$ we have $\EE{f(X_n)} \to \EE{f(X)}$.
  \end{definition}
\end{frame}

% -----------
\begin{frame}{Convergence in distributions}
  \begin{theorem}[Lévy's continuity theorem]
    For $(X_n)$ and $X$ are random vectors in $\mathbb{R}^{d}$ we have
    \[
      X_{n} \xrightarrow{\mathcal{D}} X \quad\iff\quad \varphi_{X_{n}}(t) \to \varphi_{X}(t), \forall t \in \mathbb{R}^d.
    \]
  \end{theorem}\pause
  \begin{theorem}
    If $d=1$, i.e. $X_{n}$ and $X$ are real random variables, then
    \[
      X_{n} \xrightarrow{\mathcal{D}} X \quad\iff\quad F_{X_{n}}(t) \to F_{X}(t), \forall t \in \mathcal{C}_{F_{X}},
    \]
    where $\mathcal{C}_{F_{X}}$ is the domain of continuity of $F_{X}$.
  \end{theorem}
\end{frame}

% -----------
\begin{frame}{Central limit theorem}
  \begin{proposition}
    Let $(X_{i})$ be i.i.d. random vectors of $\mathbb{R}^{d}$ with mean $\EE{X_i}=m$, then\pause
    \[
      \frac{1}{n}\sum_{i=1}^n (X_i) \xrightarrow{\mathcal{D}} m
    \]
  \end{proposition}\pause
  \emph{This proposition is true with stronger types of convergences, and then it is called \term{law of large numbers (LLN)}.}\pause
  \begin{theorem}[CLT]
    Let $(X_{i})$ be i.i.d. random vectors of $\mathbb{R}^{d}$ with mean $\EE{X_i}=m$ and covariance matrix $\cov(X_i)=\Sigma$, then\pause
    \[
      \frac{1}{\sqrt{n}}\sum_{i=1}^n (X_i-m) \xrightarrow{\mathcal{D}} \mathcal{N}(0,\Sigma).
    \]
  \end{theorem}
\end{frame}


\end{document}
