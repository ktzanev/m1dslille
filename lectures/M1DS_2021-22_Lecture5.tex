% \PassOptionsToClass{negative}{m1ds_beamer}
% \PassOptionsToClass{withsidebar}{m1ds_beamer}
% \PassOptionsToClass{handout}{m1ds_beamer}
\documentclass[11pt]{m1ds_beamer}

% ---------------
\title{M1 Data Science - Introduction to Probability}
\subtitle{Lecture 5}
%\author{Kroum Tzanev}
\date{December 7, 2021}
% ---------------

\begin{document}

% ------- The title --------
\begin{frame}
  \titlepage
\end{frame}

% ------- Warning --------
\begin{frame}
  \begin{alertblock}{Slides}
    This document contains only definitions and propositions. So it can be considered as detailed plan.\pause

    The main part --- explanations, proofs, examples, \ldots --- will be done on the blackboard.
  \end{alertblock}
\end{frame}

% =======================================
\section{Gaussian random variables}
% =======================================

% -----------
\begin{frame}{GRV : definition}
  \begin{definition}
    A continuous real random variable $X$ with density
    \[
      \rho(t) = \frac{1}{\sqrt{2\pi v}}\eexp^{-\frac{(t-m)^2}{2v}}, \quad m \in \mathbb{R}, v \in \mathbb{R}^*_+
    \]
    is called \term{Gaussian}.\\\pause
    The distribution having density $\rho$ is said to be \term{normal} or \term{Gaussian}.\\\pause
    We denote this by $X\sim \mathcal{N}(m,v)$.
  \end{definition}\pause
  \begin{block}{Convention}
    We consider a constant variable $X = m$ as Gaussian with parameters $m$ and $0$, $X\sim\mathcal{N}(m,0)$.
  \end{block}
\end{frame}

% -----------
\begin{frame}{GRV : properties}
  \begin{proposition}
    $Y\sim \mathcal{N}(m,\sigma^2)$ iff $Y=\sigma X+m$ where $X\sim \mathcal{N}(0,1)$ is a \term{standard} Gaussian variable, with density $\frac{1}{\sqrt{2\pi}}\eexp^{-\frac{t^2}{2}}$.
  \end{proposition}\pause
  \begin{proposition}
    If $Y\sim\mathcal{N}(m,v)$ then $\EE{Y}=m$ and $\VV{Y}=v$.
  \end{proposition}\pause
  \begin{proposition}
    % https://en.wikipedia.org/wiki/Sum_of_normally_distributed_random_variables
    If $Y_{1}, \dots, Y_{n}$ are \myemph{mutually independent} Gaussian random variables, $Y_{i}\sim \mathcal{N}(m_i,v_i)$, then $Y = Y_{1}+\dots+Y_{n}$ is Gaussian with $\EE{Y} = m_{1}+\dots+m_{n}$ and $\VV{Y} = v_{1}+\dots+v_{n}$
  \end{proposition}
\end{frame}

% =======================================
\section{Gaussian random vector}
% =======================================

% -----------
\begin{frame}{GRV : definition}
  \begin{definition}
    A real random vector $X =(X_{1},\ldots,X_{n})$ is called a \term{standard Gaussian random vector} if all of its components $X_{i}$ are iid standard Gaussian random variables, i.e. if all $X_{i}\sim \mathcal{N}(0,1)$ are mutually independent.
  \end{definition}\pause
  \begin{definition}
    A real random vector $Y =(Y_{1},\ldots,Y_{m})$ is called a \term{Gaussian random vector} if $Y=L(X)$ for some standard Gaussian random vector $X$ and an affine map $L:\mathbb{R}^{n}\to\mathbb{R}^{m}$, i.e. if $Y=AX+B$ with $A \in \mathbb{M}_{m,n}$ and $B \in \mathbb{R}^{m}$.\\\pause
    We say in this case that the real random variables $Y_{1},\ldots,Y_{m}$ are \term{jointly Gaussian}.
  \end{definition}
\end{frame}

% -----------
\begin{frame}{GRV : standard density function}
  \begin{proposition}
    A continuous random vector in $\mathbb{R}^{n}$ is standard Gaussian if its density function $\rho_{n}$ is given by
    %\vspace{-.3\baselineskip}
    \[
      \rho_{n}(x) = \frac{1}{\sqrt{(2\pi)^n}}\eexp^{-\frac{\norm{x}^2 }{2}} \text{ for } x \in \mathbb{R}^n.
    \]
    \vspace{-.7\baselineskip}
  \end{proposition}\pause
  \begin{proposition}
    If $X$ is a standard Gaussian random vector then in any orthonormal base the components $(X^*_1,\dots,X^*_n)$ of $X$ in this base are iid standard Gaussian variables.
  \end{proposition} \pause
  \begin{proposition}
    If $X \in \mathbb{R}^{n}$ is a standard Gaussian random vector then for any subspace $H$ the random vector $\pi_H(X)$ is a standard Gaussian random vector of $H$, where $\pi_{H}$ is the orthogonal projection on $H$.
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{GRV : density function}
  \begin{proposition}
    For a Gaussian random vector $Y=L(X)$, with $X$ standard Gaussian of dimension $n$ and $L:\mathbb{R}^{n}\to\mathbb{R}^{m}$, the following properties are equivalent:
    \begin{enumerate}[<+(1)->]
      \item The map $L$ is surjective.
      \item The covariance matrix $\Sigma$ of $Y$ is positive definite (invertible).
      \item There exists a standard Gaussian $\tilde{X}$ of dimension $m$ and a bijection $\tilde{L}:\mathbb{R}^{m}\to\mathbb{R}^{m}$ such that $Y=\tilde{L}(\tilde{X})$.
      \item $Y$ is a continuous random vector.
    \end{enumerate}\pause
    In this case the density function $\rho_Y$ of the Gaussian random vector $Y$ with expectation $\mu = \EE{Y}$ is given by
    \[
      \rho_Y(y) = \frac{1}{\sqrt{(2\pi)^m\det \Sigma}}\eexp^{-\frac{\scalprod{(y-\mu)}{\Sigma^{-1}(y-\mu)}}{2}} \text{ for } y \in \mathbb{R}^m.
    \]
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{GRV : general case}
  \begin{proposition}
    Given a Gaussian random vector $Y \in \mathbb{R}^{m}$ (with expectation $\mu$ and covariance $\Sigma$) the support of $\P_Y$ is an affine subspace $H_Y$ and
    \begin{enumerate}[<+(1)->]
      \item $H_Y = \mu + (\ker \Sigma)^{\perp} = \mu + \im \Sigma$.
      \item $Y$ is constant $\iff$  $\dim H_Y=0$ \pause$\iff$ $\Sigma = 0$.
      \item If $Y$ is non constant then it has a density when restricted to $H_Y$ with respect to the Lebesgue measure of $H_Y$, \pause i.e. if we restrict $Y$ to $H_Y$ the density is still\\
      \[
        \rho_Y(x) = \frac{1}{\sqrt{(2\pi)^k\det \Delta}}\eexp^{-\frac{\scalprod{(x-\mu)}{\Delta^{-1}(x-\mu)}}{2}} \text{ for } x \in H_Y,
      \]
      \vskip .49\baselineskip
      where $k = \dim H_{Y} \geq 1$ and $\Delta = \Sigma\vert_{\vv{H_Y}}$.
    \end{enumerate}
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{GRV : properties}
  \begin{enumerate}[<+(1)->]
    \item Gaussian vector of dimension $1$ is Gaussian rrv.
    \item If $Y=(Y_1,\dots,Y_m)$ is a Gaussian random vector then for any constants $a_1,\dots,a_m \in \mathbb{R}$ the linear combination $a_1Y_1+\dots+a_mY_m \in \mathbb{R}$ is a Gaussian random variable.
    \item The law of a Gaussian random vector $Y \in \mathbb{R}^{m}$ is completely determined by its mean $\mu \coloneqq \EE{Y} \in \mathbb{R}^{m}$ and by its covariance matrix $\Sigma \coloneqq \cov(Y) \in\mathbb{M}^+_{m}$ and we note \term{$Y\sim \mathcal{N}_m(\mu,\Sigma)$}.
    \item The components $Y_{1},\dots,Y_{m}$ of a Gaussian random vector $Y\sim \mathcal{N}_m(\mu,\Sigma)$ are independent iff $\Sigma$ is diagonal.
    \item For a Gaussian random vector $Y\sim \mathcal{N}_m(\mu,\Sigma)$ there exists an orthonormal basis such that the components $Y_{1},\dots,Y_{m}$ of $Y$ in this basis are independent random Gaussian variables.
    \item If $Y_{1},\dots,Y_{m}$ are independent random Gaussian variables, then $(Y_1,\ldots,Y_m)$ is a Gaussian random vector.
  \end{enumerate}
\end{frame}

% =======================================
\section{Gaussian characteristic function}
% =======================================

% -----------
\begin{frame}{Characteristic function of Gaussian random varaible}
  \begin{enumerate}[<+(1)->]
    \item For standard gaussian rrv $X$ we have:
    \[
      \varphi_X(t) = \eexp^{-\frac{t^2}{2}}\quad\text{ for } t\in\mathbb{R}
    \]
    \item For general gaussian rrv $X\sim\mathcal{N}(\mu,\sigma^2)$ we have:
    \[
      \varphi_X(t) = \eexp^{it\mu-\frac{t^2\sigma^2}{2}}\quad\text{ for } t\in\mathbb{R}
    \]
    \item For standard gaussian $n$-vector $V$ we have:
    \[
      \varphi_V(\xi) = \eexp^{-\frac{\norm{\xi}^2}{2}}\quad\text{ for } \xi\in\mathbb{R}^n
    \]
    \item For general gaussian $n$-vector $V\sim\mathcal{N}(\mu,\Sigma)$ we have:
    \[
      \varphi_V(\xi) = \eexp^{i\scalprod{\xi}{\mu}-\frac{\scalprod{\xi}{\Sigma\xi}}{2}}\quad\text{ for } \xi\in\mathbb{R}^n
    \]
  \end{enumerate}
\end{frame}

\end{document}
