% \PassOptionsToClass{negative}{m1ds_beamer}
% \PassOptionsToClass{withsidebar}{m1ds_beamer}
% \PassOptionsToClass{handout}{m1ds_beamer}
\documentclass[11pt]{m1ds_beamer}

% ---------------
\title{M1 Data Science - Introduction to Probability}
\subtitle{Lecture 2}
%\author{Kroum Tzanev}
\date{November 16, 2021}
% ---------------

\DeclareMathOperator{\supp}{supp}

\begin{document}

% ------- The title --------
\begin{frame}
  \titlepage
\end{frame}

% ------- Warning --------
\begin{frame}
  \begin{alertblock}{Slides}
    This document contains only definitions and propositions. So it can be considered as detailed plan.\pause

    The main part --- explanations, proofs, examples, \ldots --- will be done on the blackboard.
  \end{alertblock}
\end{frame}

% =======================================
\section{Random variables}
% =======================================

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Measurable functions}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Measurable functions : remainder}
  \begin{definition}
    Given two topological spaces $\Omega_S$ and $\Omega_T$, the function $f:\Omega_{S} \to \Omega_{T}$ is \term{measurable} if the inverse image of a Borel set is a Borel set :\pause
    $f^{-1}(A)$ is Borel subset for any Borel $A$.
  \end{definition}
  \pause
  \begin{proposition}
    The composition of measurable functions is measurable.
  \end{proposition}
  \pause
  \begin{block}{Meta-theorem}
    You can consider all sets as Borel and all functions as measurable, because you will never meet one that is not ;)
  \end{block}
\end{frame}

% -----------
\begin{frame}{Push forward of measure : reminder}

  Consider some measurable function $f:\Omega_{S}\to \Omega_{T}$ and a measure $\mu_{S}$ on $\Omega_{S}$.\pause The \term{image measure} (or \term{push-forward measure}) $\mu_T  \coloneqq f^*(\mu_S)$ on $\Omega_{T}$ is defined by any of the following equivalent conditions:
  \begin{enumerate}[<+(1)->]
    \item $\mu_T(A) = \mu_S \left( f^{-1}(A) \right)$ for all measurable $A \subset \Omega_{T}$.
    \item $\int_{\Omega_T} g(t) d\mu_T(t) = \int_{\Omega_S} g \left( f(s)  \right) d\mu_S(s)$ for any measurable $g : \Omega_{T} \to \mathbb{R}$.
  \end{enumerate}

\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Random variable : definition}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Random variable}
  Let $\left( \Omega, \mathcal{F}, \P \right)$ a probability spaces, and $\left( \Sigma, \mathcal{G} \right)$ a measurable space.\pause

  \begin{definition}
    A measurable function $V:\Omega \to \Sigma$ is called \term{random variable}.
  \end{definition}\pause
  For any event $A$ of $\Sigma$, the event \enquote{$V$ takes values in $A$} of $\Omega$ is $\left\{ V\in A \right\} \coloneqq \left\{ \omega \in \Omega \mid V(\omega) \in A \right\}\pause = V^{-1}(A)$.\pause
  \begin{proposition}
    If $V:\Omega \to \Sigma$ is a random variable, then $\P_{V}(A) \coloneqq \PP{V\in A}$ defines a probability measure on $\left( \Sigma, \mathcal{G} \right)$.
  \end{proposition}\pause
  \begin{definition}
    For a random variable $V:\Omega \to \Sigma$ we call the probability $\P_{V}$ the \term{distribution}, or the \term{law}, of $V$.
  \end{definition}
\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Types of random variable}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Real random variables}
  \begin{definition}
    A random variable $V:\Omega \to \mathbb{R}$ is called \term{real} and we abbreviate this to \term{r.r.v.} or simply \term{rrv}.
    \begin{enumerate}[<+(1)->]
      \item If $\P_{V}$ is a discrete measure we say that $V$ is \term{discrete} random variable.\pause
      This is the case for example when $V(\Omega) \subseteq \mathbb{Z}$ or when $V(\Omega)$ is finite.
      \item If $\P_{V}$ is continuous we say that $V$ is \term{continuous} random variable.
    \end{enumerate}\pause
    And we call $F_{V} \coloneqq F_{\P_V}$ the \term{cumulative distribution function} of $V$\\\pause
    and $\rho_{V} \coloneqq \rho_{F_V}$ the \term{density} of $V$.
  \end{definition}\pause
  \begin{block}{Warning}
    A continuous random variable is not necessary a continuous function, and vice-versa.
  \end{block}
\end{frame}

% -----------
\begin{frame}{Generalized inverse - computer simulation}
  Let $F$ be a non-decreasing, right-continuous functions with limits $\lim_{t \to -\infty}F(t)=0$ and $\lim_{t \to \infty}F(t)=1$.\pause
  \begin{definition}
    The \term{generalized inverse} $G:(0,1)\to\mathbb{R}$ of $F$ is defined by
    \[
      G(\omega)=\inf\setdef{x\in\mathbb{R}}{F(x)\geq\omega}\text{ for }\omega \in (0,1).
    \]
    For $F=F_{V}$ we call $G=G_{V}$ also the \term{quantile function} of $V$.
  \end{definition}
  \pause
  \begin{proposition}
    The generalized inverse $G$ of $F$ is a random variable on the uniform probability space over $(0,1)$\pause\
    with cumulative distribution function $F_{G}=F$.
  \end{proposition}
  \pause
  \begin{corollary}
    Given any real random variable $V$ there exist a random variable $U$ on the uniform probability space $(0,1)$ with the same distribution $\P_{U} = \P_{V}$.
  \end{corollary}
\end{frame}

% -----------
\begin{frame}{Random vector}
  \begin{definition}
    A random variable $V:\Omega \to \mathbb{R}^{n}$ is called a \term{random vector} (of dimension $n$).
    \pause

    In the particular case $V:\Omega \to \mathbb{M}_{m,n}$ is called a \term{random matrix} (of size $m\times n$ and dimension $mn$).
  \end{definition}
  \pause
  \begin{remark}
    Considering $n$ real random variables $V_{1},\dots,V_{n}$ is equivalent to consider a real random vector $V=(V_{1},\dots,V_{n})$ of dimension $n$.
  \end{remark}
  \pause
  \begin{definition}
    The distributions $\P_{V_{1}},\dots,\P_{V_{n}}$ of $V_{1},\dots,V_{n}$ are known as the \term{marginal distributions} of the vector $V=(V_{1},\dots,V_{n})$.
  \end{definition}
\end{frame}


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Functions of random variables}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Composition of random variables : definition}
  Consider
  \begin{enumerate}[<+(1)->]
    \item A probability spaces $\left( \mathcal{X}, \mathcal{F}, \P \right)$ and two measurable spaces $\left( \mathcal{Y}, \mathcal{G} \right)$ and $\left( \mathcal{Z}, \mathcal{H} \right)$;
    \item A random variable $V:\mathcal{X}\to\mathcal{Y}$;
    \item A measurable function $f:\mathcal{Y}\to\mathcal{Z}$;
  \end{enumerate}\pause
  \begin{proposition}
    The composition $f(V) \coloneqq f \circ V : \mathcal{X}\to\mathcal{Z}$ is a random variable.\pause

    The function $f$ itself is a random variable on $\left( \mathcal{Y}, \mathcal{G}, \P_{V} \right)$ that has the same probability distribution $P_{f} = P_{f(V)}$ as $f(V)$.
  \end{proposition}
\end{frame}


% -----------
\begin{frame}{Composition of random variables : particular cases}
  \emph{This slide reminds how push forward of measures works in two particular cases : the discrete one and the continuous one.}
  \begin{enumerate}[<+(1)->]
    \item If $V$ is a discrete we can consider $f(V)$ for any $f$. In this case we have the mass function
    \[
      p_{f(V)}(n) = \sum_{k \in f^{-1}(n)} p_{V}(k).
    \]
    \item For continuous rrv $V$ and $W=f(V)$, if $f:\supp V \xrightarrow{\sim} \supp W$ is a diffeomorphism with inverse $g:\supp W \xrightarrow{\sim} \supp V$ \pause then $W$ is continuous \pause and we have
    \[
      \rho_W(t) = \rho_V(g(t)) \abs{g'(t)}.
    \]\vspace{-\baselineskip}
  \end{enumerate}\pause
  \emph{We can iterate these constructions to consider more complicated expressions, like for example $f_1(f_2(V_1,V_2))$\dots}
\end{frame}


% -----------
\begin{frame}{Marginal distributions}
  We consider a random vector $V = (V_1,\dots,V_n)$ on $\mathbb{R}^{n}$.\pause
  \begin{proposition}
    % This is a particular case of the push forward of discrete measure.
    If $V$ is discrete with values in $X \subset \mathbb{R}^{n}$ and PMF $p_{V}$, then all the marginal distributions $p_{V_i}$ are discrete \pause with PMF
    \[
      p_{V_i}(k) \quad=\quad \sum_{\mathclap{(x_1,\dots,x_{i-1},k,x_{i+1},\dots,x_{n})\in X}} \quad p_{V}(x_1,\dots,x_{i-1},k,x_{i+1},\dots,x_{n}) \text{ for } k \in \pi_i(X),
    \]\vskip 3pt\relax
    where $\pi_{i}$ is the $i$-th projection.
  \end{proposition}\pause
  \begin{proposition}
    % This is Fubini's theorem.
    If $V$ is continuous with PDF $\rho_{V}$, then all the marginal distributions are continuous with PDF
    \[
      \rho_{V_i}(t) \quad=\quad \iiint_{\mathbb{R}^{n-1}} \quad \rho_{V}(x_1,\dots,x_{i-1},t,x_{i+1},\dots,x_{n})\dd x.
    \]
  \end{proposition}
\end{frame}

\end{document}
