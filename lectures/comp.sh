#!/bin/sh

title="M1DS_2021-22_Lecture"

# check if the parameter is in [1,9]
# =============
if [ ! -f $title$1.tex ]; then
  echo "The file $title$1.tex do not exists."
  echo "Usage : ./comp.sh <1-9>"
  exit 1
else
  echo "Compile $title$1 ..."
  echo "==================================="
fi

# clear folder
# =============
for f in temp.*; do
    ## check if temp files exist
    if [ -f "$f" ]; then
      rm temp.*
    fi
    ## This is all we needed to know, so we can break after the first iteration
    break
done

# anim version
# =============
echo ================================================
echo Build : $title$1_anim.pdf
echo ================================================
cp $title$1.tex temp.tex
tectonic temp.tex
mv temp.pdf $title$1_anim.pdf


# noanim version
# =============
echo ================================================
echo Build : $title$1_noanim.pdf
echo ================================================
# sed '1i\
# \\PassOptionsToClass{handout}{m1ds_beamer}' $title$1.tex > temp.tex
echo '\PassOptionsToClass{handout}{m1ds_beamer}' | cat - $title$1.tex > temp.tex
tectonic temp.tex
mv temp.pdf $title$1_noanim.pdf


# print version (4 in 1)
# ======================
echo ================================================
echo Build : $title$1_print.pdf
echo ================================================
# sed '1i\
# \\PassOptionsToClass{print}{m1ds_beamer}' $title$1.tex > temp.tex
echo '\PassOptionsToClass{print}{m1ds_beamer}' | cat - $title$1.tex > temp.tex
# xelatex => lualatex in 2020.11 because of some problems ...
tectonic temp.tex
mv temp.pdf $title$1_print.pdf

# clear folder
# =============
rm temp.*
