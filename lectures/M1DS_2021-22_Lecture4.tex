% \PassOptionsToClass{negative}{m1ds_beamer}
% \PassOptionsToClass{withsidebar}{m1ds_beamer}
% \PassOptionsToClass{handout}{m1ds_beamer}
\documentclass[11pt]{m1ds_beamer}

% ---------------
\title{M1 Data Science - Introduction to Probability}
\subtitle{Lecture 4}
%\author{Kroum Tzanev}
\date{November 30, 2021}
% ---------------

\DeclareMathOperator{\supp}{supp}

\begin{document}

% ------- The title --------
\begin{frame}
  \titlepage
\end{frame}

% ------- Warning --------
\begin{frame}
  \begin{alertblock}{Slides}
    This document contains only definitions and propositions. So it can be considered as detailed plan.\pause

    The main part --- explanations, proofs, examples, \ldots --- will be done on the blackboard.
  \end{alertblock}
\end{frame}

% =======================================
\section{Independence and conditional probability}
% =======================================

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Independence of events}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Independence}
  \begin{definition}
    Two events $A$ and $B$ are \term{independent} if and only if
    \[
      \PP{A \cap B} = \PP{A} \PP{B}.
    \]
  \end{definition}\pause
  \begin{definition}
    A set of events $\left(A_{i} \right)_{i\in I}$ is \term{(mutually) independent} if \textbf{every finite subset} $S \subseteq I$ of events is \emph{independent} in the sens
    \[
      \PP{\bigcap_{i \in S} A_{i}} = \prod_{i \in S} \PP{A_{i}}.
    \]
  \end{definition}\pause
  \begin{block}{Remarks}
    \begin{enumerate}[<+(1)->]
      \item Being two by two independent do not apply \emph{mutually independent} for a family.
      \item All impossible events (like $\emptyset$) and all certain events (like $\Omega$) are independent to all other events.
    \end{enumerate}
  \end{block}
\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Conditional probability}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Conditional probability: definition}
  \begin{definition}
    Given a possible event $B$, $\PP{B}\neq 0$, we define
    \[
      \P_B(A) \coloneqq \PP{A | B} \coloneqq \frac{\PP{A \cap B}}{\PP{B}},
    \]\pause
    and we interpret it as the probability of $A$ knowing that $B$ is realized.
  \end{definition}\pause
  \begin{proposition}
    $\P_B$ is a probability (measure).
  \end{proposition}\pause
  \begin{proposition}
    If $\Omega \subset{\mathbb{R}^n}$ and $\P$ the uniform probability on $\Omega$, then for any non impossible event $B$ the conditional probability $\P_{B}$ is the uniform probability on $B$.
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{Conditional probability: properties}
  Under the condition of the existence of conditional probabilities we have:
  \begin{proposition}
    \begin{enumerate}[<+(1)->]
      \item $\PP{A\cap B\cap C\cap \dots} = \PP{A}\PP{B|A}\PP{C|A\cap B}\dots$,
      \item $\P_C(A | B) = \PP{A | B \cap C} = \P_B(A | C)$,
      \item Two possible events $A$ and $B$ are independent iff
        \[
          \PP{A|B} = \PP{A}\quad\iff\quad \PP{B|A} = \PP{B}.
        \]
    \end{enumerate}\vspace{-\baselineskip}
  \end{proposition}\pause
  \begin{proposition}[Bayes' theorem]
    \begin{enumerate}[<+(1)->]
      \item $\PP{A|B} = \frac{\PP{B|A}\PP{A}}{\PP{B|A}\PP{A}+\PP{B|\bar{A}}\PP{\bar{A}}}$
      \item \myemph{(general case)} For an (almost) exhaustive system of possible events $\left( A_i \right)$ we have
      \[
        \PP{A_{1}|B} = \frac{\PP{B|A_{1}}\PP{A_{1}}}{\sum \PP{B|A_{i}}\PP{A_{i}}}.
      \]
    \end{enumerate}
  \end{proposition}
\end{frame}



% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Independence of random variables}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Definition of Independence}
  \begin{definition}
    \begin{itemize}[<+(1)->]
      \item Two random variables $V$ and $W$ are \term{independent} if \myemph{any} two events $\left\{ V \in A \right\}$  and $\left\{ W \in B \right\}$ are independent.
      \item A family $\left(V_{i}\right)$ of random variables is considered \term{(mutually) independent} if \myemph{any} family of events $\left(V_{i} \in A_i \right)$ is (mutually) independent.
    \end{itemize}
  \end{definition}\pause
  \begin{remark}
    We abbreviate \term{i.r.v.} or \term{irv} for \myemph{independent random variables}.\\\pause
    We abbreviate \term{i.i.d} or \term{iid} for \myemph{independent identically distributed (random variables)}, i.e. a family of random variables that are (mutually) independent and all has the same distribution (law).\\\pause
    This arrives for example when we repeat the same experience many times in the same conditions.
  \end{remark}
\end{frame}

% -----------
\begin{frame}{Independence : properties}

  \begin{itemize}[<+(0)->]
  \item A constant \enquote{random} variable is independent of any other random variable.
  \item If $\left(V_{i}\right)_{i=1}^{n}$ are independent then $\left(f_i(V_{i})\right)_{i=1}^{n}$ are independent.
  \item $\left(V_{i}\right)_{i=1}^{n}$ are independent \textbf{iff} the probability distribution $\P$ of the vector $\left(V_{1},\dots,V_{n} \right)$ is the product of the marginal distributions $\P=\P_{V_1}\dots\P_{V_n}$. In particular
    \begin{itemize}
      \item If $\left(V_{i}\right)_{i=1}^{n}$ are discrete with $p_i$ as PMF, then they are independent iff the PMF $p$ of $V$ is the product $p_1\dots p_n$.
      \item If $\left(V_{i}\right)_{i=1}^{n}$ are continuous with $\rho_i$ as densities, then they are independent iff the density $\rho$ of $V$ is the product $\rho_1\dots\rho_n$.
    \end{itemize}
  \item $X_1,\dots, X_n$ are \myemph{independent} real random variables \textbf{iff} for all real numbers $a_1, ..., a_n$, we have the identity
    \[
      \varphi_{a_{1}X_{1}+\cdots +a_{n}X_{n}}(t) = \varphi_{X_{1}}(a_{1}t)\cdots \varphi_{X_{n}}(a_{n}t).
    \]
    \begin{itemize}
      \item In the specific case of the sum of two \myemph{independent} random variables X and Y one has
        \[
          \varphi_{X+Y}(t)=\varphi_{X}(t)\varphi_{Y}(t).
        \]
    \end{itemize}
  \end{itemize}
\end{frame}



% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Covariance (value)}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Covariance of two real variables}
  \begin{definition}
    The \term{covariance} value of two real random variables $U$ and $V$ defined on the same probability space $(\Omega, \mathcal{F}, \P)$ is
    \[
      \cov(U,V) = \EE{(U-\EE{U})(V-\EE{V})} \quad \in \mathbb{R}
    \]
  \end{definition}
  \begin{block}{Properties}
    \begin{itemize}[<+(1)->]
      \item The covariance is a bilinear symmetric positive form (inner product),
      \item $\cov(U,U) = \VV{U}$,
      \item $\cov(U,V) = \EE{UV} - \EE{U}\EE{V}$,
      \item $\V(U+V)=\V(U)+2\cov(U,V)+\V(V)$,
      \item We say that $U$ and $V$ are \term{uncorrelated} if $\cov(U,V) = 0$,\\\pause
      iff $\EE{UV} = \EE{U}\EE{V}$, \pause
      iff $\V(U+V)=\V(U)+\V(V)$.
      \item If $U$ and $V$ are independent then they are uncorrelated.
    \end{itemize}
  \end{block}
\end{frame}


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Covariance matrix}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Covariance matrix}
  Let $U$ and $V$ be two random (column) vectors in $\mathbb{R}^{n}$ defined on the same probably space $(\Omega, \mathcal{F}, \P)$.
  \begin{definition}\pause
    The \term{covariance matrix} of two vectors $U$ and $V$ is
    \[
      \cov(U,V) = \EE{(U-\EE{U})(V-\EE{V})^T} \quad \in M_n(\mathbb{R})
    \]\pause
    The \term{\uncover<+(1)->{auto-}covariance matrix} of single vectors $U$ is
    \[
      \uncover<.(1)->{\cov(U) \coloneqq} \cov(U,U) \quad \in M_n(\mathbb{R})
    \]\pause
  \end{definition}
  \vspace{-\baselineskip}
  \begin{block}{Properties}
    \begin{itemize}[<+(1)->]
      \item $\cov(U)$ is a positive (symmetric) matrix,
      \item If $X$ and $Y$ are rrv, the for $U = \begin{psmallmatrix} X\\Y\end{psmallmatrix}$ we have $\cov(U) = \begin{psmallmatrix} \VV{X}&\cov(X,Y)\\\cov(X,Y)&\VV{Y}\end{psmallmatrix}$.
      \item $\trace(\cov(U)) = \VV{U}$.
      \item If $U = AV+B$ is an affine transform of $X$ then $\cov(U) = A\cov(V)\tr{A}$.
    \end{itemize}
  \end{block}
\end{frame}

% -----------
\begin{frame}{Covariance matrix}

  \begin{block}{Definition-Proposition}
    Two random vectors $U, V \in \mathbb{R}^{n}$ are \term{uncorrelated} if one of the following equivalent conditions is verified
    \begin{itemize}[<+(1)->]
      \item $\cov(U,V) = 0_{n} = \cov(V,U)$.
      \item Any coordinate of $U$ is uncorrelated with any coordinate of $V$.
      \item $\cov(U+V)=\cov(U)+\cov(V)$
    \end{itemize}
  \end{block}\pause
  \vspace{-\baselineskip}
  \begin{block}{Properties}
    \begin{itemize}[<+(1)->]
      \item The coordinates of $U$ are uncorrelated between them iff $\cov(U)$ is diagonal.
      \item If the coordinates of $U$ are independant between them $\cov(U)$ is diagonal.
      \item If $U$ and $V$ are independent random vectors then they are uncorrelated, and in particular $\cov(U+V)=\cov(U)+\cov(V)$.
    \end{itemize}
  \end{block}
\end{frame}

\end{document}
