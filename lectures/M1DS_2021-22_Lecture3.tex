% \PassOptionsToClass{negative}{m1ds_beamer}
% \PassOptionsToClass{withsidebar}{m1ds_beamer}
% \PassOptionsToClass{handout}{m1ds_beamer}
\documentclass[11pt]{m1ds_beamer}

% ---------------
\title{M1 Data Science - Introduction to Probability}
\subtitle{Lecture 3}
%\author{Kroum Tzanev}
\date{November 23, 2021}
% ---------------

\begin{document}

% ------- The title --------
\begin{frame}
  \titlepage
\end{frame}

% ------- Warning --------
\begin{frame}
  \begin{alertblock}{Slides}
    This document contains only definitions and propositions. So it can be considered as detailed plan.\pause

    The main part --- explanations, proofs, examples, \ldots --- will be done on the blackboard.
  \end{alertblock}
\end{frame}


% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Expected value}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Expectation : definition}
  \begin{definition}
    For a \textbf{\Alt<1>{real random variable}{random vector}} $V:\left( \Omega,\mathcal{F},\P \right) \to \mathbb{R}\Alt<1>{}{^n}$ we define the \term{expected value}, \textbf{if it exists}, as
    \[
      \EE{V} = \int_{\Omega} V(\omega) \dd\PP{\omega}.
    \]
  \end{definition}
  \pause\pause
  \begin{proposition}
    For random vector $V \in \mathbb{R}^{n}$ we have
    \[
      \EE{V} = \int_{\mathbb{R}^n} t \dd\P_V(t).
    \]\pause

    And for a measurable function $f:\mathbb{R}^n\to\mathbb{R}^m$ we have
    \[
      \EE{f(V)} = \int_{\Omega} f \left(V(\omega)\right) \dd\PP{\omega} = \int_{\mathbb{R}^n} f(t) \dd\P_V(t) = \int_{\mathbb{R}^m} t \dd\P_{f(V)}(t).
    \]
  \end{proposition}
\end{frame}


% -----------
\begin{frame}{Expectation : properties}
  \begin{enumerate}[<+(1)->]
    \item The expectation of a constant variable is the constant value, $\EE{c}=c$.
    \item If $U\sim V$ then $\EE{U}=\EE{V}$\pause, and more generally $\EE{f(U)}=\EE{f(V)}$.
    \item The expectation is positive linear functional:
    \begin{itemize}
      \item $\EE{\lambda V + \mu W} =\lambda \EE{V} + \mu \EE{W} $
      \item (monotone) $V \leq W \implies \EE{V} \leq \EE{W}$
      \item $m\leq V \leq M \implies m\leq \EE{V} \leq M$
    \end{itemize}
    \item For vectors $\EE{V_1,\dots,V_n} = \left(\EE{V_1},\dots,\EE{V_n} \right)$.
    \item If $f$ is linear/affine then $\EE{f(V)} = f \left(\EE{V}\right)$.
    \item We will see in the next lecture that the expectation of independent (non correlated) random variables is multiplicative: \pause $\EE{VW}=\EE{V}\EE{W}$ \pause if $V$ and $W$ are \myemph{independent} (or \myemph{non correlated} in the real case).\\\pause
    \emph{Here the meaning of $VW$ is not important, what is important is that $(V,W)\mapsto VW$ is bilinear.}
  \end{enumerate}
\end{frame}
% -----------
\begin{frame}{Expectation : particular cases}

  For a \myemph{discrete} random variable $V:\left( \Omega,\mathcal{F},\P \right) \to \mathbb{Z}$ with \term{ probability mass function} $p_{n} : = \PP{f=n}$ we have the formula
  \[
    \EE{V} = \sum_{n\in\mathbb{Z}} n p_n.
  \]\pause
  And if $f:\mathbb{Z}\to\mathbb{Z}$ is an arbitrary function we have
  \[
    \EE{f(V)} = \sum_{n\in\mathbb{Z}} f(n)p_n = \sum_{n\in\mathbb{Z}} n q_n.
  \]
  where $q_{n} = \PP{f(V) = n} = \sum_{i, f(i)=n} p_{n}$.
  \pause

  For a \myemph{continuous} \Alt<-4>{real random variable}{random vector} $V:\left( \Omega,\mathcal{F},\P \right) \to \mathbb{R}\Alt<-4>{}{^n}$ with density $\rho_{V}$ we have the formulas
  \[
    \EE{V} = \int_{\mathbb{R}\Alt<-4>{}{^n}} t\rho_V(t) \dd t.
  \]\pause
  And for (measurable) $f:\mathbb{R}\Alt<-4>{}{^n}\to\mathbb{R}\Alt<-4>{}{^m}$ we have
  \[
    \EE{f(V)} = \int_{\mathbb{R}\Alt<-4>{}{^n}} f(t)\rho_V(t) \dd t.
  \]\pause
\end{frame}


% -----------
\begin{frame}{Expectation and cumulative distribution}

  For a real random variable $V:\left( \Omega,\mathcal{F},\P \right) \to \mathbb{R}$ with  cumulative distribution function $F_{V}$ having $G_{V}$ as generalized inverse we have\pause
  \[
    \EE{V} = \int_{0}^{1} G_V(t) \dd t = \int_{-\infty}^{0} -F_V(t) \dd t + \int_{0}^{\infty} 1-F_V(t) \dd t.
  \]
  \pause
  So if $V:\left( \Omega,\mathcal{F},\P \right) \to \mathbb{N}$ is positive discrete we have
  \[
    \EE{V} = \sum_{n=0}^{\infty} 1-F_V(n).
  \]
\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Variance and standard deviation}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Variance : definition}
  \begin{definition}
    For a \textbf{\Alt<-2>{real random variable}{random vector}} $V:\left( \Omega,\mathcal{F},\P \right) \to \mathbb{R}\Alt<-2>{}{^n}$ we define the \Alt<-2>{\term{variance}}{\term{variance value}}, \textbf{if it exists}, as
    \[
      \VV{V} = \EE{ \Alt<-2>{\abs{V - \EE{V}}}{\norm{V - \EE{V}}}^2 } \quad\in \mathbb{R}_{+}.
    \]\pause
    And we call \term{standard deviation} its square root
    \[
      \sigma(V) = \sqrt{\VV{V}}.
    \]
  \end{definition}
  \pause\pause
  \begin{proposition}
    For a \textbf{\Alt<-4>{real random variable}{random vector}} $V:\left( \Omega,\mathcal{F},\P \right) \to \mathbb{R}\Alt<-4>{}{^n}$ we have
    \[
      \VV{V} = \Alt<-4>{\EE{V^2}}{\EE{\norm{V}^2}} - \Alt<-4>{\EE{V}^2}{\norm{\EE{V}}^2}.
    \]\pause\pause
    And for $V=(V_1,\dots,V_n)$ we have
    \[
      \VV{V} = \VV{V_1}+\cdots+\VV{V_n}.
    \]
  \end{proposition}
\end{frame}


% -----------
\begin{frame}{Variance : particular cases}
  \begin{itemize}[<+(1)->]
    \item If $V \in \mathbb{Z}$ is discrete with mass function $p_{n}$ we have
    \[
      \VV{V} = \sum_{n \in \mathbb{Z}}n^2p_n - \left(\sum_{n \in \mathbb{Z}}n p_n\right)^2.
    \]
    \item If $V \in \mathbb{R}$ is continuous with density $\rho$ we have
    \[
      \VV{V} = \int_{\mathbb{R}}t^2 \rho(t)\dd t - \left(\int_{\mathbb{R}}t \rho(t)\dd t\right)^2.
    \]
  \end{itemize}
\end{frame}

% -----------
\begin{frame}{Variance : properties}
  \begin{enumerate}[<+(1)->]
    \item $V$ is constant (almost surely) $\iff \VV{V}=0$.
    \item $\VV{V+c}=\VV{V}$.
    \item If $U\sim V$ then $\VV{U}=\VV{V}$\pause, and more generally $\VV{f(U)}=\VV{f(V)}$.
    \item If $\left( V_i \right)$ are \myemph{(two-by-two) uncorrelated} (in particular if \myemph{independent}) then
    \[
      \VV{\sum V_i} = \sum \VV{V_i}.
    \]
    \alert{(Will be seen in the next lecture.)}
    \item If $\left( V_i \right)$ are \myemph{real} and \myemph{independent} then
    \[
      \VV{\prod V_i} = \prod \EE{V_i^2} - \prod \EE{V_i}^2.
    \]
  \end{enumerate}
\end{frame}

% =======================================
\section{Characteristic Function}
% =======================================

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Characteristic Function of probability measure}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% -----------
\begin{frame}{Characteristic Function: definition}
  \begin{definition}
    The \term{characteristic function} of a random vector $X \in \mathbb{R}^{n}$ is the \enquote{fourier transform} of its law $\P_{X}$ denoted by $\varphi_{X}:\mathbb{R}^{n} \to \mathbb{D} \subset\mathbb{C}$ and given for $t \in \mathbb{R}^{n}$ by
    \[
      \varphi_{X}(t) = \int_{\mathbb{R}^n} \eexp^{i\scalprod ts} \dd\P_{X}(s) = \EE{\eexp^{i\scalprod tX}}.
    \]
  \end{definition}\pause
  \begin{proposition}
    Two random variables/vectors have the same law \textbf{iff} their characteristic functions coincide :
    \[
      X\sim Y\quad\iff\quad\varphi_{X}=\varphi_{Y}.
    \]
  \end{proposition}
\end{frame}

% -----------
\begin{frame}{Characteristic Function: properties}
  \begin{block}{Particular cases}
    \begin{enumerate}[<+->]
      \item If $X$ is integer valued (discrete) with PMF $(p_{n})_{n \in \mathbb{Z}}$ then
      \[
        \varphi_X(t) = \sum_{n\in\mathbb{Z}} \eexp^{itn}p_{n}.
      \]
      \item If $X$ is continuous real r.v. with density $\rho_{X}$ then
      \[
        \varphi_X(t) = \int_{\mathbb{R}} \eexp^{its} \rho_{X}(s)\dd s.
      \]
    \end{enumerate}
  \end{block}\pause
  \begin{block}{Properties}
    \begin{itemize}[<+->]
      \item $\varphi(0)=1$,
      \item $\abs{\varphi(t)}\leq 1$ for all $t\in \mathbb{R}$.
      \item For $Y=AX+B$ with $A \in M_{n}(\mathbb{R})$ and $B \in \mathbb{R}^{n}$ we have $\varphi_{Y}(t) = \eexp^{i\scalprod tB}\varphi_{X}(A^T t)$.\\\pause
      In particular if $X$ and $Y$ are real with $Y=aX+b$ we have $\varphi_{Y}(t) = \eexp^{itb}\varphi_{X}(at)$.
    \end{itemize}
  \end{block}
\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\subsection{Inequalities}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


% -----------
\begin{frame}{Inequalities}
  For real random variable $V$ we have:
  \begin{itemize}[<+(1)->]
    \item \myemph{(Jensen)} If $f:\mathbb{R}\to\mathbb{R}$ is convex, then $\EE{f(V)} \geq f(\EE{V})$.
    \item \myemph{(Markov)} If $V \geq 0$ and $a > 0$ then $\PP{V \geq a} \leq \frac{\EE{V}}{a}$.
    \item \myemph{(Chebyshev)} If $a > 0$ then $\PP{\abs{V-\EE{V}} \geq a} \leq \frac{\VV{V}}{a^2}$.
    \item \myemph{(Paley-Zigmund)} If $V \geq 0$ and $a \in [0,1]$ then
    \[
      (1-a^2)\frac{\EE{V}^2}{\EE{V^2}} \;\leq\; \PP{V \geq a\EE{V}}.
    \]
  \end{itemize}\pause
  For random vector $V \in \mathbb{R}^{n}$ we have:
  \begin{itemize}[<+(1)->]
    \item \myemph{(Jensen)} If $f:\mathbb{R}^{n}\to\mathbb{R}$ is convex, then $\EE{f(V)} \geq f(\EE{V})$.
    \item \myemph{(Chebyshev)} If $a > 0$ then $\PP{\norm{V-\EE{V}} \geq a} \leq \frac{\VV{V}}{a^2}$.
  \end{itemize}
\end{frame}


\end{document}
